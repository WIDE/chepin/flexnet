# flexnet

This repository contains the results presented in the article [Multisource Rumor Spreading with Network Coding](https://hal.inria.fr/hal-01946632):

> The last decade has witnessed of a rising surge interest in Gossip protocols in distributed systems. In particular, as soon as there is a need to disseminate events, they become a key functional building block due to their scalability, robustness and fault tolerance under high churn. However, Gossip protocols are known to be bandwidth intensive. A huge amount of algorithms has been studied to limit the number of exchanged messages using different combination of push/pull approaches. We are revisiting the state of the art by applying Random Linear Network Coding to further increase performances. In particular, the originality of our approach is to combine sparse vector encoding to send our network coding coefficients and Lamport timestamps to split messages in generations in order to provide an efficient gossiping. Our results demonstrate that we are able to drastically reduce the bandwidth overhead and the delay compared to the state of the art. 

## Usage

A docker container is provided, you can run it with:

```bash
docker run -t -i superboum/flexnet:v1 bash
```

You can rebuild it from scratch by running:

```bash
cd docker
docker build . -t superboum/flexnet:v1
```

If you don't want to use Docker, the `docker/Dockerfile` file will provide you the different steps to deploy the whole project on Fedora and has better chance to contain updated information than a README file that can't be automatically run.

## Reproduce article's simulation and graph

```bash
docker run -t -i -v `pwd`/pdf:flexnet/pdf superboum/flexnet:v1 bash
cd ./flexnet
make -j$(nproc) -f generate_figures.mk all
```

The final result is `pdf/figures.pdf`.
It contains the figures presented in the article.

## Development

While developping, you compile with `make` and run with `./omnet -u Cmdenv -c <target>`.
If you need more information, `./omnet -h` can help.

If you want to add a new files to the project, you must run `opp_makemake` before `make`:

```bash
opp_makemake -f -I '${HOME}/.local/include' -L '${HOME}/.local/lib' -lflexcodepp
make
```
