#include "push-pull-nc.h"

Define_Module(PushPullNC);

void cdeliver2(void* handle, unsigned char* msg, uint16_t len) {
  ((PushPullNC*)handle)->deliver(msg, len);
}

void csend2(void* handle, pool_packet_t* pkts, uint8_t len) {
  ((PushPullNC*)handle)->send(pkts, len);
}

void PushPullNC::initialize()
{
  PushPull::initialize();
  int fanout = getParentModule()->par("fanout").intValue();
  int ttl = getParentModule()->par("ttl").intValue();
  localPool.init((void*)this, cdeliver2, csend2, fanout, 1, 1, 0, POL_OCTAVE, (uint8_t)ttl);
}

void PushPullNC::inject(uint16_t idx)
{
  set_received2(idx);
  // The content of the message is also the message ID, but we suppose a 1KB message content
  o->msgid_to_generation[idx] = localPool.current_generation();
  localPool.broadcast(idx, (unsigned char*)&idx, (uint16_t)sizeof(uint16_t));
}

void PushPullNC::deliver(unsigned char* msg, uint16_t len)
{
  uint16_t num = *((uint16_t*)msg);
  set_received2(num);
}

void PushPullNC::send(pool_packet_t* pkts, uint8_t len)
{
  for (int i = 0; i < len; i++) {
    cModule *target = rps();

    pool_packet_t pp;
    pool_packet_deep_cpy(&pp, &(pkts[i]));
    
    NCPacket* np = new NCPacket("nc-push");
    np->setNcPkt(pp);

    sent_packet_push(1000+1+5*pp.coefs_count+4);
    sendDirect(np, std::max(0., normal(o->latency_avg, o->latency_stddev)), 0., target, "in");
  }
}

void PushPullNC::forward(cMessage *msg) {
  if (strcmp(msg->getName(), "nc-push") == 0) {
    push((NCPacket*) msg);
  } else {
    PushPull::forward(msg);
  }
}

void PushPullNC::push(NCPacket *m)
{
  std::vector<uint16_t> remoteWindow;
  pool_packet_t p = m->getNcPkt();
  for (int i = 0; i < p.coefs_count ; i++) {
    remoteWindow.push_back(p.coefs[i].msg_id);
  } 
  miss->add(remoteWindow);
  localPool.receive(&p);
  pool_packet_free(&p);
  delete m;
}

void PushPullNC::pullReply(PullReplyPacket* msg)
{
  uint16_t rcv_id = msg->getMissingPktId();
  miss->add(msg->getTradingWindow());
  delete msg;

  if (rcv_id == UINT16_MAX || is_received(rcv_id)) { 
    prev_useless++;
  } else {
    prev_useful++;
    
    genkv_coef_t coefs;
    coefs.msg_id = rcv_id;
    coefs.coef_value = 1;

    pool_packet_t pp;
    pp.gid = o->msgid_to_generation[rcv_id];
    pp.ttl = 0; // We don't want to forward the packet
    pp.coefs_count = 1;
    pp.coefs = &coefs;
    pp.vals_count = sizeof(uint16_t);
    pp.vals = (uint8_t*) &rcv_id;
    
    localPool.receive(&pp);
  }
}

void PushPullNC::finish()
{
  if (getIndex() != 0) return;

  cOutVector genv("Generation Sizes Vector");
  cStdDev gen("Generation Sizes");
  for (auto const &size : localPool.stat_generations_size()) {
    if (size == 0) continue;
    genv.record(size);
    gen.collect(size);
  }
  recordScalar("Number of generations", gen.getCount());
  recordScalar("Mean generation sizes", gen.getMean());
  recordScalar("Standard deviation of generation sizes", gen.getStddev());
  recordScalar("Minimum generation size", gen.getMin());
  recordScalar("Maximum generation size", gen.getMax());
}
