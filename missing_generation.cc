#include "missing_generation.h"
#include <algorithm>

MissingGeneration::MissingGeneration(PushPullNC* dm, uint16_t gen_shift)
: dm(dm), gen_shift(gen_shift)
{}

MissingGeneration::~MissingGeneration() {}

void MissingGeneration::check_generation()
{
  for (uint32_t g = gen_shift; g <= dm->localPool.current_generation(); g++) {
    uint32_t target_gen = g - gen_shift;
    if (will_miss.find(target_gen) == will_miss.end()) continue;
    for (auto const& id : will_miss[target_gen])
    {
      computed_missing.push_back(id);
    }
    will_miss[target_gen].clear();
    will_miss.erase(target_gen);
  }
}

std::size_t MissingGeneration::size()
{
  check_generation();
  return computed_missing.size();
}

uint64_t MissingGeneration::add(std::vector<uint16_t>& a)
{
  for (auto const& id : a) {
    if (dm->is_received(id) || is_in(id)) continue;
    auto gen_id = dm->o->msgid_to_generation[id];
    will_miss[gen_id].push_back(id);
  }
  return 0;
}

uint8_t MissingGeneration::received(uint16_t id)
{
  auto gen_id = dm->o->msgid_to_generation[id];
  if (will_miss.find(gen_id) != will_miss.end()) {
    auto iter = std::find(will_miss[gen_id].begin(), will_miss[gen_id].end(), id);
    if (iter != will_miss[gen_id].end()) {
      will_miss[gen_id].erase(iter);
      return 1;
    } 
  }

  auto iter = std::find(computed_missing.begin(), computed_missing.end(), id);
  if (iter != computed_missing.end()) {
    computed_missing.erase(iter);
  }
  return 0;
}

bool MissingGeneration::is_in(uint16_t id)
{
  auto gen_id = dm->o->msgid_to_generation[id];
  if (will_miss.find(gen_id) != will_miss.end()) {
    auto iter = std::find(will_miss[gen_id].begin(), will_miss[gen_id].end(), id);
    if (iter != will_miss[gen_id].end()) {
      return true;
    } 
  }

  auto iter = std::find(computed_missing.begin(), computed_missing.end(), id);
  if (iter != computed_missing.end()) {
    return true;
  }
  return false;
}

void MissingGeneration::populate(std::vector<uint16_t>& a)
{
  check_generation();

  /*
  std::cout << "list: ";
  for (auto const& ent : computed_missing) {
    std::cout << ent << ", ";
  }
  std::cout << std::endl;
  */

  if (computed_missing.size() > 0) {
    std::rotate(computed_missing.begin(), computed_missing.begin()+1, computed_missing.end());
  }
  
  for (auto const& miss_id : computed_missing) {
    a.push_back(miss_id);
  }
}
