#include "missing_map.h"

MissingMap::MissingMap(DisseminationModule* dm)
: dm(dm), shift(0)
{}

std::size_t MissingMap::size()
{
  return data.size();
}

bool MissingMap::is_in(uint16_t id)
{
  return data.find(id) != data.end();
}

void MissingMap::add(std::map<uint32_t, std::vector<uint16_t>>& a)
{
  for (auto const& ent : a) {
    for (auto const& msgid : ent.second) {
      if (dm->is_received(msgid) || msgid == UINT16_MAX) continue;
      data[msgid] = ent.first;
    }
  }
}

void MissingMap::received(uint16_t id)
{
  auto it = data.find(id);
  if (data.find(id) != data.end()) {
    data.erase(it);
  }
}

void MissingMap::populate(std::vector<uint32_t>& a)
{
  std::set<uint32_t> cont;
  for (auto const& ent : data) {
    if (cont.find(ent.second) == cont.end()) {
      a.push_back(ent.second);
      cont.insert(ent.second);
    }
  }

  #if MMAP_ROTATE
  if (cont.size() == 0) return;
  shift = shift % a.size(); // Stay in the vector whatever its size is
  std::rotate(a.begin(), a.begin()+shift, a.end());
  shift++;
  #endif
}

MissingMap::~MissingMap()
{
}
