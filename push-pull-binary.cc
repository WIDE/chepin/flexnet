#include "push-pull-binary.h"

// The module class needs to be registered with OMNeT++
Define_Module(PushPullBinary);

PushPullBinary::PushPullBinary()
{
  miss = new MissingSimple(this);
}

PushPullBinary::~PushPullBinary()
{
  delete miss;
}

void PushPullBinary::initialize()
{
  DisseminationModule::initialize();

  prev_missing_size = 0;
  prev_useless = 0;
  prev_useful = 0;
  last_missing = 0;
  deltaPull = getParentModule()->par("rounds_delta").doubleValue();
  //deltaPull = uniform(0.01,1);

  /** Adaptive **/
  rcv_samples = 0;
  samples = 80;
  cpid = 0;
  useful_count = 0;
  total = 0;
  lastAdaptation = simTime();
  state = bin_init;
  binary_min = std::numeric_limits<double>::min();
  binary_max = std::numeric_limits<double>::max();
  usefulness_interval = 0.1;
  usefulness_target = 0.75;
  usefulness_min = 1;
  usefulness_max = 0;

  tradingLength = getParentModule()->par("trading_length").intValue();
  tradingMargin = getParentModule()->par("trading_margin").intValue();

  cMessage* clock = new cMessage("periodic-pull");
  scheduleAt(uniform(0, deltaPull), clock);
  
  cMessage* swi = new cMessage("switch-freq");
  scheduleAt(300, swi);
  rate.setName("rate");

  #if LOG_USEFUL_USELESS
  useful.setName("useful");
  useless.setName("useless");
  #endif

  #if LOG_FREQ
  reset_freq.setName("reset_freq");
  increase_freq.setName("increase_freq");
  decrease_freq.setName("decrease_freq");
  current_freq.setName("current_freq");
  #endif

  #if LOG_USELESSNESS
  useless_unknown.setName("useless_unknown");
  useless_redundant.setName("useless_redundant");
  #endif
  
  #if MISSING_LOG
  missing_log.setName("missing_log");
  #endif
}

void PushPullBinary::periodicPull()
{
  PullPacket* p = new PullPacket("pull");
  p->setKind(1);
  p->setPullId(cpid);
  p->setSentTime(simTime().dbl());
  miss->populate(p->getMissing());
  populateTradingWindow(p->getTradingWindow());

  auto target = rps();
  //double latency = std::max(0., normal(o->latency_avg, o->latency_stddev));
  double latency = o->get_latency(getIndex(), target->getIndex());
  
  sendDirect(p, latency, 0., target, "in");
  #if DEBUG_SENT_PULL
  std::cout << "Missing size: " << p->getMissing().size() << "(" << p->getMissing().size() * MSG_ID_SIZE << " bytes)" << std::endl;
  std::cout << "Trading size: " << p->getTradingWindow().size() << "(" << p->getTradingWindow().size() * MSG_ID_SIZE << " bytes)" << std::endl;
  #endif
  sent_packet_pull(p->getMissing().size() * MSG_ID_SIZE + p->getTradingWindow().size() * MSG_ID_SIZE);
}

void PushPullBinary::set_received2(uint16_t id) 
{
  // Add packet to recent history
  recent_history.push_back(id);
  if (recent_history.size() > tradingLength + tradingMargin) {
    recent_history.pop_front();
  }

  // Remove it from missing list
  miss->received(id); 

  // Log it as received
  set_received(id);
}

void PushPullBinary::inject(uint16_t id)
{
  PushPacket* msg = new PushPacket("push");
  msg->setPktId(id);
  msg->setTtl(o->ttl);
  msg->setKind(0);
  forward(msg);
}

void PushPullBinary::forward(cMessage *msg) {
  if (is_down) {
    delete msg;
    return;
  }
  
  if (strcmp(msg->getName(), "push") == 0) {
    push((PushPacket*)msg);
  } else if (strcmp(msg->getName(), "pull") == 0) {
    pull((PullPacket*)msg);
  } else if (strcmp(msg->getName(), "reply-pull") == 0) {
    pullReply((PullReplyPacket*)msg);
  }
}

void PushPullBinary::push(PushPacket* msg)
{
  uint16_t pkt_id = msg->getPktId();
  
  // Look at trading window and add new missing packets id
  miss->add(msg->getTradingWindow());
  #if MISSING_LOG
  missing_log.record(miss->size());
  #endif
  
  // Look if we have already received the packet
  if (is_received(pkt_id)) {
    delete msg;
    return;
  }

  // If not, add the packet to the received list and the recent history list
  set_received2(pkt_id);

  if (msg->getTtl() <= 0) {
    delete msg;
    return;
  }
  // Decrement the TTL
  msg->setTtl(msg->getTtl() - 1);

  // The TTL is not 0, packet will be forwarded
  // Update the trading window with a part of our recent_history
  msg->getTradingWindow().clear();
  populateTradingWindow(msg->getTradingWindow());

  for (int i = 0; i < o->fanout; i++) {
    auto target = rps();
    //double latency = std::max(0., normal(o->latency_avg, o->latency_stddev));
    double latency = o->get_latency(getIndex(),target->getIndex());
    sendDirect(msg->dup(), latency, 0., target, "in");
    sent_packet_push(1000+ MSG_ID_SIZE +1+msg->getTradingWindow().size() * MSG_ID_SIZE);
  }
  
  delete msg; 
}

void PushPullBinary::populateTradingWindow(std::vector<uint16_t>& trading) {
  for (auto it = recent_history.begin();
       it != recent_history.end() - std::min(tradingMargin, recent_history.size());
       ++it) {
    trading.push_back(*it);
  }
}

void PushPullBinary::pull(PullPacket* msg)
{
  miss->add(msg->getTradingWindow());
  #if MISSING_LOG
  missing_log.record(miss->size());
  #endif

  // Find first missing packet
  uint16_t selected_id = UINT16_MAX;
  for (auto const& ent : msg->getMissing()) {
    if (is_received(ent)) {
      selected_id = ent;
      break;
    }
  }
  
  PullReplyPacket* pr = new PullReplyPacket("reply-pull");
  pr->setMissingPktId(selected_id);
  pr->setKind(2);
  pr->setSentTime(msg->getSentTime());
  pr->setPullId(msg->getPullId());
  populateTradingWindow(pr->getTradingWindow());
 
  auto target = msg->getSenderModule();
  //double latency = std::max(0., normal(o->latency_avg, o->latency_stddev));
  double latency = o->get_latency(getIndex(), target->getIndex());
  sendDirect(pr, latency, 0., target, "in");
  if (selected_id == UINT16_MAX) {
    sent_packet_pull_reply(MSG_ID_SIZE + pr->getTradingWindow().size() * MSG_ID_SIZE);
  } else {
    sent_packet_pull_reply(MSG_ID_SIZE + 1000 + pr->getTradingWindow().size() * MSG_ID_SIZE);
  }
  delete msg;
}

void PushPullBinary::pullReply(PullReplyPacket* msg)
{
  uint16_t rcv_id = msg->getMissingPktId();
  miss->add(msg->getTradingWindow());
  #if MISSING_LOG
  missing_log.record(miss->size());
  #endif
  uint8_t mpid = msg->getPullId();
  delete msg;

  bool is_useful = false;
  if (rcv_id == UINT16_MAX || is_received(rcv_id)) {
    #if LOG_USEFUL_USELESS
    useless.record(1);
    #endif

    #if LOG_USELESSNESS
    if (rcv_id == UINT16_MAX) useless_unknown.record(1);
    else useless_redundant.record(1);
    #endif
  } else {
    is_useful = true;
    #if LOG_USEFUL_USELESS 
    useful.record(1);
    #endif

    set_received2(rcv_id);
  }

  total++;
  if (is_useful) useful_count++;
  if (cpid == mpid) rcv_samples++;
  if (samples == rcv_samples) {
    double usefulness = (double)useful_count / (double)total;
    double oldFreq = 1 / (double) deltaPull.dbl();
    double newFreq = 0;
    bool reloop = true;
    
    while (reloop) {
      switch (state) {
        case bin_init:
          if (getIndex() == 0) std::cout << "init" << std::endl;
          if (usefulness > usefulness_target + usefulness_interval) {
            usefulness_min = usefulness;
            binary_min = oldFreq;
            binary_max = std::numeric_limits<double>::max();
            newFreq = 2 * oldFreq;
            state = bin_search;
            reloop = false;
          } else if (usefulness < usefulness_target - usefulness_interval) {
            binary_min = std::numeric_limits<double>::min();
            binary_max = oldFreq;
            usefulness_max = usefulness;
            newFreq = 0.5 * oldFreq;
            state = bin_search;
            reloop = false;
          } else {
            state = bin_stable;
          }

          break;

        case bin_search:
          if (getIndex() == 0) std::cout << "search" << std::endl;
          if (usefulness > usefulness_target - usefulness_interval && usefulness < usefulness_target + usefulness_interval) {
            state = bin_stable;
          } else if (binary_min == std::numeric_limits<double>::min() && usefulness >= usefulness_max) {
            if (usefulness > usefulness_target + usefulness_interval) {
              //binary_min = oldFreq;
              //usefulness_min = usefulness;
              state = bin_reduce;
            } else {
              usefulness_max = usefulness;
              binary_max = oldFreq;
              newFreq = 0.5 * oldFreq;
              reloop = false;
            }
          } else if (binary_max == std::numeric_limits<double>::max() && usefulness <= usefulness_min) {
            if (usefulness < usefulness_target - usefulness_interval) {
              //binary_max = oldFreq;
              //usefulness_max = usefulness;
              state = bin_reduce;
            } else {
              usefulness_min = usefulness;
              binary_min = oldFreq;
              newFreq = 2 * oldFreq;
              reloop = false;
            }
          } else {
            state = bin_init;
          }
          break;

        case bin_reduce:
          if (getIndex() == 0) std::cout << "reduce" << std::endl;

          if (usefulness > usefulness_target - usefulness_interval && usefulness < usefulness_target + usefulness_interval) {
            state = bin_stable;
          } else if (usefulness >= usefulness_min || usefulness <= usefulness_max || usefulness_min - usefulness_max < 2 * usefulness_interval) {
            state = bin_init;
          } else {
            if (usefulness > usefulness_target + usefulness_interval) {
              binary_min = oldFreq;
              usefulness_min = usefulness;
            } else {
              binary_max = oldFreq;
              usefulness_max = usefulness;
            }
            newFreq = (binary_max - binary_min) / 2 + binary_min;
            reloop = false;
          }
          break;

        case bin_stable:
          if (getIndex() == 0) std::cout << "stable" << std::endl;
          if (usefulness > usefulness_target + usefulness_interval
              || usefulness < usefulness_target - usefulness_interval) {
            state = bin_init;
          } else {
            newFreq = oldFreq;
            reloop = false;
          }
          break;
      }
    }

    newFreq = std::min(1000.0, std::max(newFreq, 0.01));
    if (getIndex() == 0)
      std::cout << "freq=" << newFreq
        << ", usefulness=" << usefulness
        << ", useful_min=" << usefulness_min
        << ", useful_max=" << usefulness_max
        << ", freq_max=" << binary_max
        << ", freq_min=" << binary_min
        << std::endl;

    rate.record(newFreq);
    deltaPull = 1/newFreq;

    cpid++;
    rcv_samples = 0;
    useful_count = 0;
    total = 0;
  }

}

void PushPullBinary::background(cMessage* msg)
{
  if (strcmp(msg->getName(), "periodic-pull") == 0) {
    if (!is_down) periodicPull();
    scheduleAt(simTime()+deltaPull, msg);
  } else if (strcmp(msg->getName(), "switch-freq") == 0) {
    //deltaPull *= 2;
    delete msg;
  }
}
