#include "missing_lincom.h"
#include "push-pull-trading-gen.h"

void MissingLinCom::add(std::map<uint32_t, std::vector<uint16_t>> trading) {
  for (auto const& entry : trading) {
    pool_packet_t pp;
    pp.gid = entry.first;
    pp.ttl = 0;
    pp.vals_count = 0;
    pp.vals = NULL;
    pp.coefs_count = entry.second.size();
    genkv_coef_t coef_list[pp.coefs_count];
    pp.coefs = coef_list;
    for (int i = 0; i < pp.coefs_count; i++) {
      /* used to debug */
      /*
      auto real_gen = dm->o->msgid_to_generation[entry.second[i]];
      if (real_gen != entry.first) {
        std::cout << "ERROR !!!!!" << std::endl;
	std::cout << entry.second[i] << " belongs to " << real_gen << " and not " << entry.first << std::endl;
      }
      */
      auto msgid = entry.second[i];
      if (dm->o->rcv_instead_dlv) {
        if (seen.find(msgid) == seen.end()) {
          dm->update_trading(msgid);
	  seen.insert(msgid);
        }
      }
      pp.coefs[i].msg_id = msgid;
      pp.coefs[i].coef_value = 0;
    }
    dm->localPool.receive(&pp);
    inform(entry.first); 
  }
}

void MissingLinCom::add_packet(pool_packet_t& pp) {
    if (dm->o->rcv_instead_dlv) {
      for (int i = 0; i < pp.coefs_count; i++) {
        auto msgid = pp.coefs[i].msg_id;
        if (seen.find(msgid) == seen.end()) {
          dm->update_trading(msgid);
	  seen.insert(msgid);
        }
      }
    }
    inform(pp.gid); 
}

void MissingLinCom::add_msg(uint32_t gid, uint16_t mid) {
  if (dm->o->rcv_instead_dlv) {
    if (seen.find(mid) == seen.end()) {
      dm->update_trading(mid);
      seen.insert(mid);
    }
  }
  inform(gid);
}

/**
 * Need to be called after received or tradingWindow
 * tradingWindow MUST be added as a 0 0 0 coefficients line
 */
void MissingLinCom::inform(uint32_t gid) {
   auto gen = dm->localPool.get_gen(gid);
   uint64_t known = gen->known_ids_count;

   uint64_t rank;
   if (dm->o->l2_instead_l1) {
     rank = gen->gen.real_rank;
   } else {
     rank = gen->gen.delivered_count;
   }

   if (report.find(gid) == report.end()) {
     missing_gen.push_back(gid);
   }
   uint64_t missing = known - rank;
   //std::cout << known << " " << rank << " " << missing << std::endl;
   report[gid] = missing;

   if (missing == 0) {
     auto gid_pos = std::find(missing_gen.begin(), missing_gen.end(), gid);
     if (gid_pos != missing_gen.end()) {
       missing_gen.erase(gid_pos);
     }
     report.erase(gid);
   }
}

uint64_t MissingLinCom::size() {
  uint64_t total_size = 0;
  for (auto const& entry : report) {
    total_size += entry.second;
  }
  return total_size;
}

void MissingLinCom::populate(std::vector<uint32_t>& a) {
  a.insert(a.begin(), missing_gen.begin(), missing_gen.end());

  if (missing_gen.size() < 2) return;
  std::rotate(missing_gen.begin(), missing_gen.begin()+1, missing_gen.end());
}
