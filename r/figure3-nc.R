source("./figure3Lib.R")

msg_count2 <- read.csv("../data/fig3_nc_msgcount.csv")
data_count2 <- read.csv("../data/fig3_nc_msgdata.csv")
delays2 <- read.csv("../data/fig3_nc_latency.csv")
g4 <- make_g1(msg_count2,F,"Packet ratio: 2.267")
g5 <- make_g2(data_count2,F, "Bandwidth ratio: 1.842")
g6 <- make_g3(delays2,F, paste("max: ", sqldf("select round(max(delay),1) from delays2 where sent_time <= 6"), "s, mean: 0.55s",sep = ""))

tikz(file='../tex/figure3-nc.tex', width = 7, height = 1, pointsize=8)
plot_grid(g4,g5,g6,ncol=3,nrow=1,align="h")
dev.off()

