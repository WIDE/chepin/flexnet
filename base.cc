#include "base.h"

void DisseminationModule::initialize()
{
  o = (Oracle*)getParentModule()->getSubmodule("oracle");
  sent_messages = 0;
  sent_data = 0; 
  sent_push = 0;
  sent_push_data = 0;
  sent_pull = 0;
  sent_pull_data = 0;
  sent_pull_reply = 0;
  sent_pull_reply_data = 0;
  #if LOG_EXCHANGE_OVER_TIME
  v_sent_messages.setName("v_sent_messages");
  v_sent_data.setName("v_sent_data");
  v_sent_push.setName("v_sent_push");
  v_sent_push_data.setName("v_sent_push_data");
  v_sent_pull.setName("v_sent_pull");
  v_sent_pull_data.setName("v_sent_pull_data");
  v_sent_pull_reply.setName("v_sent_pull_reply");
  v_sent_pull_reply_data.setName("v_sent_pull_reply_data");
  #endif
  int messages = getParentModule()->par("messages").intValue();
  bm = (uint64_t*)malloc(sizeof(uint64_t) * (messages / 64 + 1));
  memset(bm, 0, sizeof(uint64_t) * (messages / 64 + 1));
}

void DisseminationModule::sent_packet(uint32_t bytes)
{
  sent_messages++;
  sent_data += bytes;
  #if LOG_EXCHANGE_OVER_TIME
  v_sent_messages.record(1);
  v_sent_data.record(bytes);
  #endif
}

void DisseminationModule::sent_packet_push(uint32_t bytes)
{
  sent_push++;
  sent_push_data += bytes;
  sent_packet(bytes);
  #if LOG_EXCHANGE_OVER_TIME
  v_sent_push.record(1);
  v_sent_push_data.record(bytes);  
  #endif
}

void DisseminationModule::sent_packet_pull(uint32_t bytes)
{
  sent_pull++;
  sent_pull_data += bytes;
  sent_packet(bytes);
  #if LOG_EXCHANGE_OVER_TIME
  v_sent_pull.record(1);
  v_sent_pull_data.record(bytes);
  #endif
}

void DisseminationModule::sent_packet_pull_reply(uint32_t bytes)
{
  sent_pull_reply++;
  sent_pull_reply_data += bytes;
  sent_packet(bytes);
  #if LOG_EXCHANGE_OVER_TIME
  v_sent_pull_reply.record(1);
  v_sent_pull_reply_data.record(bytes);
  #endif
}

cModule* DisseminationModule::rps()
{
  int idx = uniform(0,o->count);
  if (idx == getIndex()) idx = (idx + 1) % o->count;
  return getParentModule()->getSubmodule("node",idx);
}

void DisseminationModule::set_received(uint16_t pos) {
  simtime_t delta = simTime() - o->emit_time[pos];

  if (o->banned_emit_time.find(o->emit_time[pos]) == o->banned_emit_time.end()) {
    o->delay.collect(delta);
    #if LOG_DELAY_BY_MSG
    o->delay_by_msg.push_back(std::pair<int,double>(pos, simTime().dbl()));
    #endif
  }

  uint16_t idx = pos / 64;
  uint64_t shift = pos % 64;
  bm[idx] |= (uint64_t)1 << shift;
}

bool DisseminationModule::is_received(uint16_t pos) {
  uint16_t idx = pos / 64;
  uint8_t shift = pos % 64;
  uint16_t max_idx = o->messages / 64;
  if (idx > max_idx) return false;
  return (bm[idx] & (uint64_t)1 << shift) != 0;
}

int DisseminationModule::has_missed2() {
  int start = 0;
  int missed = 0;
  if (o->churn_multiplier > 0) start = int(0.9 * o->count);
  for (int i = start; i < o->messages; i++) {
    if (is_received(i)) continue;
    if (o->never_sent.find(i) != o->never_sent.end()) continue;
    if (o->banned_emit_time.find(o->emit_time[i]) != o->banned_emit_time.end()) continue;
    // std::cout << getIndex() << " missed " << i << " emitted at " << o->emit_time[i] << std::endl;
    missed++;
  }
  return missed;
}

bool DisseminationModule::has_missed() {
  uint16_t idx = o->messages / 64;
  uint8_t shift = o->messages % 64;

  for (int i = 0; i < idx; i++) {
    if (bm[i] != UINT64_MAX) {
      /*
      for (int j = 0; j < 64; j++) {
        if (!is_received(i*64+j)) {
          std::cerr << getIndex() << " missed " << i*64+j << " (total: " << o->messages << ", idx: " << idx << ")" <<std::endl;
	}
      }
      */
      return true;
    }
  }
  
  for (int i = 0; i < shift; i++) {
    if (!is_received(idx*64+i)) {
      // std::cerr << getIndex() << " missed at " << i << std::endl;
      return true;
    }
  }

  return false;
}

DisseminationModule::~DisseminationModule() {
  free(bm);
}

void DisseminationModule::handleMessage(cMessage *msg) {
  if (strcmp(msg->getName(), "inject") == 0) {
    SimplePacket* sm = (SimplePacket*) msg;
    uint16_t idx = sm->getPktId();
    if (is_down) o->never_sent.insert(idx);
    else inject(idx);
    delete msg;
  } else if (strcmp(msg->getName(), "U") == 0) {
    is_down = false;
    delete msg;
  } else if (strcmp(msg->getName(), "D") == 0) {
    is_down = true;
    delete msg;
  } else if (msg->isSelfMessage()) {
    background(msg);
  } else {
    forward(msg);
  }
}

void DisseminationModule::background(cMessage *msg) {}

