#include <string.h>
#include <algorithm>
#include <omnetpp.h>
#include <stdint.h>
#include <map>
#include "simple_packet_m.h"
#include "base.h"

using namespace omnetpp;

class InfectDie : public DisseminationModule
{
  protected:
    virtual void inject(uint16_t id) override;
    virtual void forward(cMessage *msg) override;
};

// The module class needs to be registered with OMNeT++
Define_Module(InfectDie);

void InfectDie::inject(uint16_t id)
{
  SimplePacket* p = new SimplePacket("s");
  p->setPktId(id);
  forward(p);
}

void InfectDie::forward(cMessage *msg)
{
  SimplePacket* p = (SimplePacket*)msg;

  #if LOG_REDUNDANCY
  uint64_t id = p->getPktId();
  if (redundancy.find(id) == redundancy.end()) redundancy[id] = 0;
  redundancy[id]++;
  #endif

  if (!is_received(p->getPktId())) {
    set_received(p->getPktId());
    for (int i = 0; i < o->fanout; i++) {
      cModule *targetModule = rps();
      sendDirect(p->dup(), std::max(0., normal(o->latency_avg, o->latency_stddev)), 0., targetModule, "in");
      sent_packet(1000+4);
    }
  }
  delete p;
}
