#pragma once
#include <flexcode/cpp/pool.hpp>
#include <omnetpp.h>
#include <string.h>
#include <stdint.h>
#include <iterator>
#include <deque>
#include "build_config.h"
#include "base.h"
#include "missing_lincom.h"
#include "push_pull_m.h"
#include "nc_packet_m.h"
extern "C" {
#include "flexcode/c/pool.h"
#include "flexcode/c/generation_kv.h"
}

using namespace omnetpp;

class PushPullTradingGen : public DisseminationModule
{
  public:
    PushPullTradingGen() { miss = new MissingLinCom(this); }
    ~PushPullTradingGen() { delete miss; }

    virtual void deliver(unsigned char* msg, uint16_t len);
    virtual void send(pool_packet_t* pkts, uint8_t len);
    
    void update_trading(uint16_t id);

    uint64_t tradingLength;
    uint64_t tradingMargin;

    Pool localPool;
  protected:
    std::deque<uint16_t> recent_history;
    MissingLinCom* miss;
    int union_coef;
    
    simtime_t deltaPull; //deltaPull
    simtime_t deltaFreq; //deltaAdjust

    #if LOG_USEFUL_USELESS
    cOutVector useful;
    cOutVector useless;
    #endif

    #if LOG_FREQ
    cOutVector reset_freq;
    cOutVector increase_freq;
    cOutVector decrease_freq;
    cOutVector current_freq;
    #endif
    
    #if LOG_USELESSNESS
    cOutVector useless_unknown;
    cOutVector useless_redundant;
    #endif
  
    #if MISSING_LOG
    cOutVector missing_log;
    #endif

    uint32_t prev_missing_size;
    uint32_t prev_useful;
    uint32_t prev_useless;

    virtual void initialize() override;
    virtual void inject(uint16_t id) override;
    virtual void forward(cMessage *msg) override;
    virtual void background(cMessage *msg) override;
    virtual void finish() override;

    void adaptFrequency();
    void periodicPull();
    
    void pull(GenPullPacket* msg);
    void pullReply(NCPacket3* msg);
    void push(NCPacket3* msg);

    void set_received2(uint16_t id);
    uint64_t populateTradingWindow(std::map<uint32_t, std::vector<uint16_t>>& trading);
};
