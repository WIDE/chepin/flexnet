#pragma once
#include <algorithm>
#include <vector>
#include <map>
#include <set>
#include "base.h"
#define MMAP_ROTATE 1

class MissingMap
{
  protected:
    DisseminationModule* dm;
  public:
    std::map<uint16_t, uint32_t> data;
    uint64_t shift;
    MissingMap(DisseminationModule* dm);
    virtual std::size_t size();
    virtual void add(std::map<uint32_t, std::vector<uint16_t>>& a);
    virtual void received(uint16_t id);
    virtual bool is_in(uint16_t id);
    virtual void populate(std::vector<uint32_t>& a);
    virtual ~MissingMap();
};
