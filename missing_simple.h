#pragma once
#include <algorithm>
#include <omnetpp.h>
#include "missing.h"
#include "base.h"
#include "build_config.h"

using namespace omnetpp;

class MissingSimple : public Missing
{
  protected:
    bool shuffle;
    uint32_t tick;
    uint32_t rotate_every;
    DisseminationModule* dm;
    std::vector<uint16_t> data;
    #if MISSING_LOG
    cOutVector missing_log;
    #endif
  public:
    MissingSimple(DisseminationModule* dm);
    void set_rotation(uint32_t rotation);
    void set_shuffle(bool shuf);
    virtual std::size_t size() override;
    virtual uint64_t add(std::vector<uint16_t>& a) override;
    virtual uint8_t received(uint16_t id) override;
    virtual bool is_in(uint16_t id) override;
    virtual void populate(std::vector<uint16_t>& a) override;
    virtual ~MissingSimple() override;
};
