#include "oracle.h"

Define_Module(Oracle);

void Oracle::initialize() {
  delay.setName("Delay");

  count = getParentModule()->par("count");
  fanout = getParentModule()->par("fanout");
  messages = getParentModule()->par("messages");
  ttl = getParentModule()->par("ttl");
  rounds_delta = getParentModule()->par("rounds_delta").doubleValue();
  messages_per_second = getParentModule()->par("messages_per_second").doubleValue();
  latency_avg = getParentModule()->par("latency_avg").doubleValue();
  latency_stddev = getParentModule()->par("latency_stddev").doubleValue();
  latency_type = getParentModule()->par("latency_type").stdstringValue();
  
  l2_instead_l1 = getParentModule()->par("l2_instead_l1").intValue();
  rcv_instead_dlv = getParentModule()->par("rcv_instead_dlv").intValue();
  churn_multiplier = getParentModule()->par("churn_multiplier").doubleValue();
  latency_multiplier = getParentModule()->par("latency_multiplier").doubleValue();

  std::set<simtime_t> injected;
  double next = 0;
  for (uint16_t i = 0; i < messages; i++) {
    cModule *targetModule = getParentModule()->getSubmodule("node",uniform(0, count));
    SimplePacket *sm = new SimplePacket("inject");
    sm->setPktId(i);
    #if SEND_SAME_STEP
    double sched_at = (double)i / messages_per_second;
    #elif SEND_INCR_STEP
    double sched_at = next;
    // next += 1 / (double)(i*0.5+1) * (1 / messages_per_second);
    if (i < 250 || i > messages - 250) next += 1 / messages_per_second;
    else next += 0.01 / messages_per_second; 
    #else
    double sched_at = uniform(0, (double)messages / messages_per_second);
    #endif
    SimTime sched_at_simtime(sched_at);
    emit_time.push_back(sched_at_simtime);
    sendDirect(sm, sched_at, 0., targetModule, "in");
    injected.insert(sched_at);
  }
  #if SEND_INCR_STEP
  double last_sent = next;
  #else
  double last_sent = messages / messages_per_second;
  #endif

  std::cout << "Will load 490 nodes interaction latencies" << std::endl;
  latencies.resize(490);
  for (int i = 0; i < latencies.size(); i++) latencies[i].resize(490);

  std::ifstream latencies_file;
  latencies_file.open("./PlanetLabData_1");
  double temp_lat;
  double avg_lat = 0; 
  for (int i = 0; i < latencies.size(); i++) {
    for (int j = 0; j < latencies[0].size(); j++) {
      if (latency_type.compare("average") == 0) {
        if(i+j == 0) std::cout << "Latency type: average" << std::endl;
        temp_lat = uniform(20, 274);
        // Uniform with lower bound = Planetlab 5th percentile and average = Planetlab average
      } else if (latency_type.compare("median") == 0) {
        if(i+j == 0) std::cout << "Latency type: median" << std::endl;
        temp_lat = 135.01; // Planetlab datatet median
      } else {
        if(i+j == 0) std::cout << "Latency type: normal (planetlab)" << std::endl;
        latencies_file >> temp_lat;
      }
      latencies[i][j] = temp_lat / 1000;
      avg_lat += temp_lat / 1000 / latencies.size() / latencies[0].size();
    }
  }
  latencies_file.close();
  std::cout << "Latencies have been loaded" << std::endl;

  double expire = (double)messages / messages_per_second  + 100 * latency_avg;
  //double expire = last_sent + 16; 
  //double expire = last_sent * 6;
  if (getParentModule()->par("need_timeout").intValue()) {
    cMessage* msg = new cMessage("end");
    scheduleAt(expire,msg);
  }

  // We first setup all the nodes to UP
  for(int i = 0; i < count; i++) {
    cModule *targetModule = getParentModule()->getSubmodule("node",i);
    ((DisseminationModule*)targetModule)->is_down = false;
  }

  if (churn_multiplier > 0) {
    std::ifstream churn_file;
    churn_file.open("./churn/results/churn_all.churn");
    
    std::string node_token, churn_type;
    int node_value, churn_time, churn_sent = 0;

    while (!churn_file.fail()) {
      churn_file >> node_token >> node_value;
      do {
        churn_file >> churn_time >> churn_type;
	      if (churn_type[0] != 'F' && !churn_file.fail()) {
	        double comput_churn_time = double(churn_time) * churn_multiplier;
	        if (comput_churn_time < expire) {
            cMessage* msg = new cMessage(churn_type.c_str());
            cModule *targetModule = getParentModule()->getSubmodule("node",node_value);
            sendDirect(msg, comput_churn_time, 0., targetModule, "in");
	          churn_sent++;
            // Then we set nodes that are affected by the churn to DOWN
            // As nodes are considered down
            ((DisseminationModule*)targetModule)->is_down = true;
	        }
	      }
      } while(churn_type[0] != 'F');
    }

    std::cout << "Sent " << double(churn_sent) / expire << " churn actions / sec" << std::endl;
  }

  auto it1 = injected.begin();
  std::advance(it1, (int)(.90 * injected.size()));
  banned_emit_time.insert(it1, injected.end()); 

  #if DEBUG_INJECT_TIME
  for (auto it = it1 ; it != injected.end(); ++it) {
    std::cout << "last injected: " << *it << std::endl;
  }
  #endif
}

double Oracle::get_latency(int from, int to) {
  return latencies[from%490][to%490] * latency_multiplier;
}

void Oracle::handleMessage(cMessage* msg)
{
  if (strcmp(msg->getName(), "end") == 0) {
    delete msg;
    endSimulation();
  }
}

void Oracle::finish()
{
  int missed = 0;
  double avg_receive = 0.;
  uint64_t total = 0;
  uint64_t total_data = 0;

  uint64_t total_push = 0;
  uint64_t total_push_data = 0;
  uint64_t total_pull = 0;
  uint64_t total_pull_data = 0;
  uint64_t total_pull_reply = 0;
  uint64_t total_pull_reply_data = 0;

  #if LOG_DELAY_BY_MSG
  cOutVector del[messages];
  for (unsigned int i = 0; i < messages; i++) {
    del[i].setName(("delay_msg_"+std::to_string(i)).c_str());
  }
  for (auto const& elem: delay_by_msg) {
    del[elem.first].recordWithTimestamp(elem.second, elem.second - emit_time[elem.first]);
  }
  #endif

  #if LOG_ATOMIC
  double recv_rate = 0;
  double atomic_rate = 0;
  for (int i = 0; i < messages; i++) {
    uint64_t current_message_counter = 0;
    for (int j = 0; j < count; j++) {
      DisseminationModule *txc = (DisseminationModule*)getParentModule()->getSubmodule("node",j);
      if (txc->is_received(i)) current_message_counter++;
    }
    recv_rate += current_message_counter / (double)count;
    if (current_message_counter == count) atomic_rate++;
  }

  recv_rate /= messages;
  atomic_rate /= messages;


  recordScalar("recv rate", recv_rate);
  recordScalar("atomic rate", atomic_rate);
  #endif

  for (int i = 0; i < count; i++) {
    DisseminationModule *txc = (DisseminationModule*)getParentModule()->getSubmodule("node",i);
    int node_missed = txc->has_missed2();
    if (node_missed) missed++;
    //int total_messages = messages - banned_emit_time.size() - never_sent.size();
    //avg_receive = avg_receive + (double)(total_messages - node_missed) / (double)(total_messages);
    total += txc->sent_messages;
    total_data += txc->sent_data;
    total_push += txc->sent_push;
    total_push_data += txc->sent_push_data;
    total_pull += txc->sent_pull;
    total_pull_data += txc->sent_pull_data;
    total_pull_reply += txc->sent_pull_reply;
    total_pull_reply_data += txc->sent_pull_reply_data;
  }

  #if LOG_REDUNDANCY
  std::map<uint64_t, uint64_t> histo;
  for (int i = 0; i < count; i++) {
    DisseminationModule *txc = (DisseminationModule*)getParentModule()->getSubmodule("node",i);
    uint64_t count_recv = 0;
    for (auto const& x : txc->redundancy) {
      count_recv++;
      uint64_t c = x.second;
      histo[c]++;
    }
    histo[0] += messages - count_recv;
  }

  std::ofstream fredundancy;
  fredundancy.open("redundancy.txt");
  for (auto const& x : histo) {
    fredundancy << x.first << "," << x.second << std::endl;  
  }
  fredundancy.close();
  #endif
 
  recordScalar("Nodes that missed at least a message", missed);
  //recordScalar("Percentage of packets received by nodes in average", avg_receive / (double)(count));
  recordScalar("Transferred messages in the network", total);
  recordScalar("Transferred bytes in the network", total_data);
  recordScalar("Transferred push in the network", total_push);
  recordScalar("Transferred push bytes in the network", total_push_data);
  recordScalar("Transferred pull messages in the network", total_pull);
  recordScalar("Transferred pull bytes in the network", total_pull_data);
  recordScalar("Transferred pull reply messages in the network", total_pull_reply);
  recordScalar("Transferred pull reply bytes in the network", total_pull_reply_data);

  recordScalar("Mean message delay", delay.getMean());
  recordScalar("Standard deviation message delay", delay.getStddev());
  recordScalar("Minimum message delay", delay.getMin());
  recordScalar("Maximum message delay", delay.getMax());
}
