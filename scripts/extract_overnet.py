#!/usr/bin/python3

import sys,csv

times = []
for line in sys.stdin:
    v1, v2 = line.split()
    if v1 == "node":
        continue

    if v2 == "F":
        continue

    times.append((int(v1), v2))

nodes = 100
writer = csv.writer(sys.stdout)
writer.writerow(["time", "nodes"])
writer.writerow([0, nodes])
for ts, act in sorted(times):
    if act == 'D':
        nodes -= 1
    elif act == 'U':
        nodes += 1
    writer.writerow([ts, nodes])
    if nodes > 1000:
        raise Exception("Too many nodes")
