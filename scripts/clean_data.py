#!/usr/bin/python3
import sys, re

titles = []
values = []
first_time = True

for line in sys.stdin:
  if "run,type,module,name,attrname,attrvalue,value" in line:
    values = []
  elif re.match("Exported \d+ scalars", line):
    if first_time:
        first_time = False
        print(','.join(titles))
    print(','.join(values))
  elif "iterationvars" in line or "iterationvarsf" in line or "measurement" in line:
    pass
  else:
    t, v = None, None
    t1, t2, v1, v2 = line.split(",")[-4:]
    if t1:
      t = t1
      v = v2
    else:
      t = t2.split(".")[-1]
      v = v1

    t = t.strip().replace(" ", "_").lower()
    if first_time:
      titles.append(t)
      values.append(v.strip())
    elif t in titles:
      values.append(v.strip())

