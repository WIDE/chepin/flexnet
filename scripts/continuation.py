def start(func):
  def wrapper(*args):
    f = func(*args)
    while callable(f): f = f()
    return f
  return wrapper

def lazy(func):
  def wrapper(*args,**kwargs):
    return lambda: func(*args, **kwargs)
  return wrapper
