import sys, csv, functools

#acc = []
counter = 0
writer = csv.writer(sys.stdout, delimiter = ',')
writer.writerow(["id","latency"])
for line in sys.stdin:
    a = [float(x) for x in line.split() if float(x) != 0]
    for e in a:
        #acc.append(e)
        writer.writerow([counter, e])
        counter += 1

#acc.sort()
#print("med", acc[int(len(acc) / 2)])
#print("avg", functools.reduce(lambda b,c: b+c, acc) / len(acc))
