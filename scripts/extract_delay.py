#!/usr/bin/python3

import omnet_parser, sys, csv, numpy, functools, traceback

count = int(sys.argv[1])
put_avg = False
if len(sys.argv) > 2:
  put_avg = True

keys = ["delay_msg_"+str(i) for i in range(count)]
params = {'filter_key': keys}
res = omnet_parser.do(sys.stdin, params)

csvdumper = csv.writer(sys.stdout, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
csvdumper.writerow(["msg_id", "sent_time", "delay", "percentile"])

for k in keys:
  try:
    d0 = res[k][0]
    d5 = res[k][int(5./100. * count)]
    d25 = res[k][int(25./100. * count)]
    d50 = res[k][int(50./100. * count)]
    d75 = res[k][int(75./100. * count)]
    d95 = res[k][int(95./100. * count)]
    dmax = res[k][-1]

    obj = [("5th", d5), ("25th", d25), ("50th", d50), ("75th", d75), ("95th", d95), ("max", dmax)]

    if put_avg:
      davg = [d0[0],d0[1], sum([x[2] for x in res[k]]) / len(res[k])]
      obj.append(("avg",davg))

    for n, v in obj:
      csvdumper.writerow([k, d0[1], v[2], n])
  except:
    pass
