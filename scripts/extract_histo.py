#!/usr/bin/python3
import omnet_parser, sys, csv, numpy, functools

params = {'filter_key': sys.argv[2:]}
res = omnet_parser.do(sys.stdin, params)

time={}
weight={}
for k in params['filter_key']:
  time[k] = list(map(lambda x: x[1], res[k]))
  weight[k] = list(map(lambda x: x[2], res[k]))

bornes = [
  functools.reduce(lambda a, x: min(a, min(x)), [y for _, y in time.items()], float("inf")),
  functools.reduce(lambda a, x: max(a, max(x)), [y for _, y in time.items()], 0)
]

csvdumper = csv.writer(sys.stdout, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
csvdumper.writerow(["time","count","key"])
whole_duration = bornes[1] - bornes[0]
duration_per_bins = whole_duration / int(sys.argv[1])
for k in params['filter_key']:
  count, start_time = numpy.histogram(time[k], bins=int(sys.argv[1]), range=bornes, weights=weight[k])
  for i in range(len(count)):
    csvdumper.writerow([start_time[i],count[i] / duration_per_bins,k])
