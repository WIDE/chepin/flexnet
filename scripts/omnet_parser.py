import continuation

@continuation.lazy
def garbage(it, line, params):
  elements = line.split()
  if len(elements) < 1 or elements[0] != "vector":
    return garbage(it, next(it), params)
  else:
    return index(it, line, params)

@continuation.lazy
def index(it, line, params):
  elements = line.split()
  if len(elements) > 0 and elements[0] == "vector":
    params['id_to_key'][elements[1]] = elements[3]
    node_str = ''.join(filter(str.isdigit, elements[2]))
    params['id_to_node'][elements[1]] = int(node_str) if len(node_str) > 0 else 0
    if not params['filter_key'] or elements[3] in params['filter_key']:
      params['key_data'][elements[3]] = []
    return index(it, next(it), params)
  else:
    return data(it, line, params)

@continuation.lazy
def data(it, line, params):
  if line == None:
    return

  elements = line.split()
  
  if len(elements) > 0 and elements[0] == "vector":
      return index(it,line,params)

  if len(elements) < 4:
    return data(it, next(it, None), params)
 
  key = params['id_to_key'][elements[0]]
  node = params['id_to_node'][elements[0]]
  time = float(elements[2])
  value = float(elements[3])
  if key in params['key_data']:
    params['key_data'][key].append([node, time, value])
  return data(it, next(it, None), params)

def do(stream, params):
  params['id_to_key'] = {}
  params['id_to_node'] = {}
  params['key_data'] = {}
  continuation.start(garbage)(stream, next(stream), params)
  return params['key_data']
