./flexnet -u Cmdenv -c pulp-orig-deep

./scripts/extract_histo.py 400 v_sent_messages v_sent_push v_sent_pull v_sent_pull_reply \
	< ./results/pulp-orig-deep-#0.vec \
	> ./scripts/parsed1.csv

./scripts/extract_histo.py 400 v_sent_data v_sent_push_data v_sent_pull_data v_sent_pull_reply_data \
	< results/pulp-orig-deep-#0.vec \
	> ./scripts/parsed2.csv

./scripts/extract_delay.py 1000 \
	< ./results/pulp-orig-deep-#0.vec \
	> ./scripts/parsed3.csv

./flexnet -u Cmdenv -c pulp-nc-deep

./scripts/extract_histo.py 400 v_sent_messages v_sent_push v_sent_pull v_sent_pull_reply \
	< ./results/pulp-nc-deep-#0.vec \
	> ./scripts/parsed4.csv

./scripts/extract_histo.py 400 v_sent_data v_sent_push_data v_sent_pull_data v_sent_pull_reply_data \
	< results/pulp-nc-deep-#0.vec \
	> ./scripts/parsed5.csv

./scripts/extract_delay.py 1000 \
	< ./results/pulp-nc-deep-#0.vec \
	> ./scripts/parsed6.csv

#### internals ######

./scripts/extract_histo.py 400 useful useless \
	< results/pulp-nc-deep-#0.vec \
	> ./scripts/parsed7.csv
	
./scripts/extract_histo.py 400 current_freq \
	< results/pulp-nc-deep-#0.vec \
	> ./scripts/parsed8.csv

./scripts/extract_data.py missing_log \
	< results/pulp-nc-deep-#0.vec \
	> ./scripts/parsed9.csv

./scripts/extract_histo.py 400 reset_freq increase_freq decrease_freq \
	< results/pulp-nc-deep-#0.vec \
	> ./scripts/parsed10.csv

#### 

./scripts/extract_histo.py 400 useful useless \
	< results/pulp-orig-deep-#0.vec \
	> ./scripts/parsed11.csv	
	
./scripts/extract_histo.py 400 current_freq \
	< results/pulp-orig-deep-#0.vec \
	> ./scripts/parsed12.csv

./scripts/extract_data.py missing_log \
	< results/pulp-orig-deep-#0.vec \
	> ./scripts/parsed13.csv

./scripts/extract_histo.py 400 reset_freq increase_freq decrease_freq \
	< results/pulp-orig-deep-#0.vec \
	> ./scripts/parsed14.csv

####

./flexnet -u Cmdenv -c pulp-variant1-deep

./scripts/extract_histo.py 400 useful useless \
	< results/pulp-variant1-deep-#0.vec \
	> ./scripts/parsed15.csv
		
./scripts/extract_histo.py 400 current_freq \
	< results/pulp-variant1-deep-#0.vec \
	> ./scripts/parsed16.csv

./scripts/extract_data.py missing_log \
	< results/pulp-variant1-deep-#0.vec \
	> ./scripts/parsed17.csv

./scripts/extract_histo.py 400 reset_freq increase_freq decrease_freq \
	< results/pulp-variant1-deep-#0.vec \
	> ./scripts/parsed18.csv

####

./flexnet -u Cmdenv -c pulp-variant2-deep

./scripts/extract_histo.py 400 useful useless \
	< results/pulp-variant2-deep-#0.vec \
	> ./scripts/parsed19.csv

./scripts/extract_histo.py 400 current_freq \
	< results/pulp-variant2-deep-#0.vec \
	> ./scripts/parsed20.csv

./scripts/extract_data.py missing_log \
	< results/pulp-variant2-deep-#0.vec \
	> ./scripts/parsed21.csv

./scripts/extract_histo.py 400 reset_freq increase_freq decrease_freq \
	< results/pulp-variant2-deep-#0.vec \
	> ./scripts/parsed22.csv

#######

opp_runall ./flexnet -c pulp-orig-rate
opp_runall ./flexnet -c pulp-nc-rate

./network_evol pulp-orig-rate evts "algo,param,atomic,bytes" > scripts/parsed23.csv
./network_evol pulp-nc-rate evts >> scripts/parsed23.csv

opp_runall ./flexnet -c pulp-orig-latency
opp_runall ./flexnet -c pulp-nc-latency

./network_evol pulp-orig-latency lmul "algo,param,atomic,bytes" > scripts/parsed24.csv
./network_evol pulp-nc-latency lmul >> scripts/parsed24.csv

opp_runall ./flexnet -c pulp-orig-size
opp_runall ./flexnet -c pulp-nc-size

./network_evol pulp-orig-size nodes "algo,param,atomic,bytes" > scripts/parsed25.csv
./network_evol pulp-nc-size nodes >> scripts/parsed25.csv


########## CHURN ###############

opp_runall ./flexnet -c pulp-orig-churn

./scripts/extract_delay.py 1000 \
	< results/pulp-orig-churn-cm=0.0005-#0.vec \
	> ./scripts/parsed26.csv

./scripts/extract_delay.py 1000 \
	< results/pulp-orig-churn-cm=0.001-#0.vec \
	> ./scripts/parsed27.csv

./scripts/extract_delay.py 1000 \
	< results/pulp-orig-churn-cm=0.002-#0.vec \
	> ./scripts/parsed28.csv

opp_runall ./flexnet -c pulp-nc-churn

./scripts/extract_delay.py 1000 \
	< results/pulp-nc-churn-cm=0.0005-#0.vec \
	> ./scripts/parsed29.csv

./scripts/extract_delay.py 1000 \
	< results/pulp-nc-churn-cm=0.001-#0.vec \
	> ./scripts/parsed30.csv

./scripts/extract_delay.py 1000 \
	< results/pulp-nc-churn-cm=0.002-#0.vec \
	> ./scripts/parsed31.csv




