#!/usr/bin/python3

import omnet_parser, sys, csv, numpy, functools

params = {'filter_key': sys.argv[1:]}
res = omnet_parser.do(sys.stdin, params)

csvdumper = csv.writer(sys.stdout, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
csvdumper.writerow(["node", "time","count","key"])

for k in sys.argv[1:]:
  for r in res[k]:
    csvdumper.writerow(r + [k])
