#include <string.h>
#include <algorithm>
#include <omnetpp.h>
#include <stdint.h>
#include <vector>
#include <flexcode/cpp/pool.hpp>
#include "nc_packet_m.h"
#include "base.h"

using namespace omnetpp;

class InfectDieNC : public DisseminationModule
{
  protected:
    virtual void initialize() override;
    virtual void inject(uint16_t id) override;
    virtual void forward(cMessage *msg) override;
    virtual void finish() override;
    std::map<uint64_t, uint64_t> gen_redundancy;
  public:
    Pool localPool;
    void deliver(unsigned char* msg, uint16_t len);
    void send(pool_packet_t* pkts, uint8_t len);
};

// The module class needs to be registered with OMNeT++
Define_Module(InfectDieNC);

void cdeliver(void* handle, unsigned char* msg, uint16_t len) {
  ((InfectDieNC*)handle)->deliver(msg, len);
}

void csend(void* handle, pool_packet_t* pkts, uint8_t len) {
  ((InfectDieNC*)handle)->send(pkts, len);
}

void InfectDieNC::initialize()
{
  DisseminationModule::initialize();
  int fanout = getParentModule()->par("fanout").intValue();
  localPool.init((void*)this, cdeliver, csend, fanout, 1, 1, 0, POL_OCTAVE,0);
}

void InfectDieNC::deliver(unsigned char* msg, uint16_t len)
{
  uint16_t num = *((uint16_t*)msg);
  set_received(num);
}

void InfectDieNC::send(pool_packet_t* pkts, uint8_t len)
{
  for (int i = 0; i < len; i++) {
    
    cModule *target = rps();

    pool_packet_t pp;
    pool_packet_deep_cpy(&pp, &(pkts[i]));
    
    NCPacket* np = new NCPacket("n");
    np->setNcPkt(pp);

    sent_packet(1000+5*pp.coefs_count+4);
    sendDirect(np, std::max(0., normal(o->latency_avg, o->latency_stddev)), 0., target, "in");
  }
}

void InfectDieNC::forward(cMessage *msg)
{
  NCPacket* m = (NCPacket*)msg;
  pool_packet_t p = m->getNcPkt();
  #if LOG_REDUNDANCY
  if (gen_redundancy.find(p.gid) == gen_redundancy.end()) gen_redundancy[p.gid] = 0;
  gen_redundancy[p.gid]++;
  #endif
  localPool.receive(&p);
  pool_packet_free(&p);
  delete msg;
}

void InfectDieNC::inject(uint16_t idx)
{
  set_received(idx);
  localPool.broadcast((unsigned char*)&idx, (uint16_t)sizeof(uint16_t));
}

void InfectDieNC::finish()
{
  #if LOG_REDUNDANCY
  for (auto const& x : gen_redundancy) {
    auto gen = localPool.get_gen(x.first);
    auto rank = genkv_rank(gen);

    for (int i = 0; i < gen->known_ids_count; i++) {
      redundancy[gen->known_ids[i]] = (double)x.second / (double)rank;
    }
  }
  #endif

  if (getIndex() != 0) return;

  cOutVector genv("Generation Sizes Vector");
  cStdDev gen("Generation Sizes");
  for (auto const &size : localPool.stat_generations_size()) {
    if (size == 0) continue;
    genv.record(size);
    gen.collect(size);
  }
  recordScalar("Number of generations", gen.getCount());
  recordScalar("Mean generation sizes", gen.getMean());
  recordScalar("Standard deviation of generation sizes", gen.getStddev());
  recordScalar("Minimum generation size", gen.getMin());
  recordScalar("Maximum generation size", gen.getMax());
}
