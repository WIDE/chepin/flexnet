#pragma once
#include <vector>
#include <stdint.h>

class Missing
{
  public:
    virtual std::size_t size() = 0;
    virtual uint64_t add(std::vector<uint16_t>& a) = 0;
    virtual uint8_t received(uint16_t id) = 0;
    virtual bool is_in(uint16_t id) = 0;
    virtual void populate(std::vector<uint16_t>& a) = 0;
    virtual ~Missing();
};
