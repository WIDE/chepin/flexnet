#include <string.h>
#include <algorithm>
#include <omnetpp.h>
#include <stdint.h>
#include <deque>
#include <set>
#include <iterator>
#include <flexcode/cpp/pool.hpp>
#include "simple_packet_m.h"
#include "nc_packet_m.h"
#include "push-pull-nc.h"
#include "missing_generation.h"
#define GENSHIFT 8

using namespace omnetpp;

class PushPullNC2 : public PushPullNC
{
  protected:
    virtual void forward(cMessage *msg) override;
    virtual void periodicPull() override;
    virtual void pull(PullPacket* msg) override;
    void pullReply(NCPacket* msg);
  public:
    PushPullNC2();
    ~PushPullNC2();
};

Define_Module(PushPullNC2);

PushPullNC2::PushPullNC2()
{
  delete miss;
  miss = new MissingGeneration(this, GENSHIFT);
}

PushPullNC2::~PushPullNC2()
{
}

void PushPullNC2::forward(cMessage *msg)
{
  if (strcmp(msg->getName(), "pull") == 0) {
    pull((PullPacket*) msg);
  } else if (strcmp(msg->getName(), "nc-reply-pull") == 0) {
    pullReply((NCPacket*) msg);
  } else {
    PushPullNC::forward(msg);
  }
}

void PushPullNC2::periodicPull()
{
  PullPacket* p = new PullPacket("pull");
  p->setKind(1);
  miss->populate(p->getMissing());
  if (p->getMissing().size() == 0) {
    delete p;
    return;
  }
  
  double latency = std::max(0., normal(o->latency_avg, o->latency_stddev));
  sendDirect(p, latency, 0., rps(), "in");
  sent_packet_pull(p->getMissing().size() * MSG_ID_SIZE);
}

void PushPullNC2::pull(PullPacket* msg)
{
  // Find first uncomplete generation
  uint16_t selected_generation = std::max(1u, localPool.current_generation()) - 0;
  for (auto const& ent : msg->getMissing()) {
    // Packets in missing are partly received, so we can encode something
    if (is_received(ent) || miss->is_in(ent)) {
      // To save memory, we keep a common lookup table in the Oracle object
      // It works as we check before that we have some knowledge of this message
      // After that, we just need to recode a message from our local generation, considering only messages we know
      selected_generation = o->msgid_to_generation[ent];
      break;
    }
  }
  delete msg;

  NCPacket* pr = new NCPacket("nc-reply-pull");
  pool_packet_t pp = {0};
  localPool.recode(&pp, selected_generation, 0);
  pr->setNcPkt(pp);
  
  sent_packet_pull_reply(1000+1+5*pp.coefs_count+3);
  sendDirect(pr, std::max(0., normal(o->latency_avg, o->latency_stddev)), 0., rps(), "in");
}

void PushPullNC2::pullReply(NCPacket* msg)
{
  pool_packet_t p = msg->getNcPkt();
  
  std::vector<uint16_t> remoteWindow;
  for (int i = 0; i < p.coefs_count ; i++) {
    remoteWindow.push_back(p.coefs[i].msg_id);
  }
  miss->add(remoteWindow);
  
  if(p.coefs_count > 0 && localPool.receive(&p)) prev_useful++;
  else prev_useless++;

  pool_packet_free(&p);
  delete msg;
}
