#include "missing_simple.h"

MissingSimple::MissingSimple(DisseminationModule* dm)
: dm(dm), tick(1), rotate_every(1), shuffle(false)
{
}

std::size_t MissingSimple::size()
{
  return data.size();
}

void MissingSimple::set_rotation(uint32_t rotation) {
  rotate_every = rotation;
}

void MissingSimple::set_shuffle(bool shuf) {
  shuffle = shuf;
}

bool MissingSimple::is_in(uint16_t id)
{
  auto id_iter = std::find(data.begin(), data.end(), id);
  return id_iter != data.end();
}

uint64_t MissingSimple::add(std::vector<uint16_t>& a)
{
  uint64_t count = 0;
  for (auto const& ent : a) {
    if (dm->is_received(ent) || ent == UINT16_MAX) continue;
    if (std::find(data.begin(), data.end(), ent) == data.end()) {
      data.push_back(ent);
      count++;
    }
  }
  return count;
}

uint8_t MissingSimple::received(uint16_t id)
{
  auto id_iter = std::find(data.begin(), data.end(), id);
  if (id_iter != data.end()) {
    data.erase(id_iter);
    return 1;
  }
  return 0;
}

void MissingSimple::populate(std::vector<uint16_t>& a)
{
  if (shuffle) {
    std::random_shuffle(data.begin(), data.end());
  } else if (data.size() > 0 && --tick == 0) {
    std::rotate(data.begin(), data.begin()+1, data.end());
    tick = rotate_every;
  }

  for (auto const& miss_id : data) {
    a.push_back(miss_id);
  }
}

MissingSimple::~MissingSimple()
{
}
