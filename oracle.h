#pragma once
#include <string.h>
#include <fstream>
#include <omnetpp.h>
#include <stdint.h>
#include <vector>
#include <map>
#include <set>
#include <utility>
#include <iterator>
#include "build_config.h"
#include "base.h"
#include "simple_packet_m.h"

using namespace omnetpp;

class Oracle : public cSimpleModule
{
  protected:
    virtual void initialize() override;
    virtual void finish() override;
    virtual void handleMessage(cMessage* msg) override;
  public:
    std::vector<simtime_t> emit_time;
    cStdDev delay;
    std::map<uint32_t, uint32_t> msgid_to_generation;
    std::set<simtime_t> banned_emit_time;
    int count;
    int fanout;
    int messages;
    int ttl;
    int l2_instead_l1;
    int rcv_instead_dlv;
    double latency_multiplier;
    double churn_multiplier;

    std::vector<std::vector<double>> latencies;
    std::set<uint16_t> never_sent;
    double rounds_delta;
    double messages_per_second;
    double latency_avg;
    double latency_stddev;
    std::string latency_type;
    double get_latency(int from, int to);
    #if LOG_DELAY_BY_MSG
    std::vector<std::pair<int, double>> delay_by_msg;
    #endif
};
