flexnet: *.cc *.h
	make -j$(shell nproc)

## FIGURE 2 SIMULATION

# @TODO

## FIGURE 3 SIMULATION

results/pulp-orig-deep-\#0.vec: flexnet omnetpp.ini dissemination.ned
	./flexnet -u Cmdenv -c pulp-orig-deep

results/pulp-nc-deep-\#0.vec: flexnet omnetpp.ini dissemination.ned
	./flexnet -u Cmdenv -c pulp-nc-deep

## FIGURE 4 SIMULATION

results/pulp-variant1-deep-\#0.vec: flexnet omnetpp.ini dissemination.ned
	./flexnet -u Cmdenv -c pulp-variant1-deep

results/pulp-variant2-deep-\#0.vec: flexnet omnetpp.ini dissemination.ned
	./flexnet -u Cmdenv -c pulp-variant2-deep

## FIGURE 5 SIMULATION

pulp-%-rate: ;
pulp-%-latency: ;
pulp-%-size: ;
EQ = =

test-%: pulp-%-rate

results/pulp-%-rate-evts$(EQ)50,ltype$(EQ)average-\#0.sca: pulp-%-rate flexnet omnetpp.ini dissemination.ned
	opp_runall ./flexnet -c $<

results/pulp-%-latency-ltype$(EQ)average,lmul$(EQ)0.1-\#0.sca: pulp-%-latency flexnet omnetpp.ini dissemination.ned
	opp_runall ./flexnet -c $<

results/pulp-%-size-nodes$(EQ)100,ltype$(EQ)average-\#0.sca: pulp-%-size flexnet omnetpp.ini dissemination.ned
	opp_runall ./flexnet -c $<

## FIGURE 6 SIMULATION

results/pulp-orig-churn-%.vec: flexnet omnetpp.ini dissemination.ned
	opp_runall ./flexnet -c pulp-orig-churn

results/pulp-nc-churn-%.vec: flexnet omnetpp.ini dissemination.ned
	opp_runall ./flexnet -c pulp-nc-churn

## EXTRACT DATA FOR FIGURE 2

# @TODO

## EXTRACT DATA FOR FIGURE 3

data/fig3_%_msgcount.csv: results/pulp-%-deep-\#0.vec
	mkdir -p ./data
	python3 ./scripts/extract_histo.py 400 v_sent_push v_sent_pull v_sent_pull_reply v_sent_messages < $< > $@

data/fig3_%_msgdata.csv: results/pulp-%-deep-\#0.vec
	mkdir -p ./data
	python3 ./scripts/extract_histo.py 400 v_sent_push_data v_sent_pull_data v_sent_pull_reply_data < $< > $@

data/fig3_%_latency.csv: results/pulp-%-deep-\#0.vec
	mkdir -p ./data
	python3 ./scripts/extract_delay.py 1000 < $< > $@

## EXTRACT DATA FOR FIGURE 4

data/fig4_%_useful.csv: results/pulp-%-deep-\#0.vec
	mkdir -p ./data
	python3 ./scripts/extract_histo.py 400 useful useless < $< > $@

data/fig4_%_curfreq.csv: results/pulp-%-deep-\#0.vec
	mkdir -p ./data
	python3 ./scripts/extract_histo.py 400 current_freq < $< > $@

data/fig4_%_missinglog.csv: results/pulp-%-deep-\#0.vec
	mkdir -p ./data
	python3 ./scripts/extract_data.py missing_log < $< > $@

#data/fig4_%_actfreq.csv: results/pulp-%-deep-\#0.vec
#	mkdir -p ./data
#	python3 ./scripts/extract_histo.py 400 reset_freq increase_freq decrease_freq < $< > $@

## EXTRACT DATA FOR FIGURE 5

data/fig5_rate.csv: \
	results/pulp-orig-rate-evts$(EQ)50,ltype$(EQ)average-\#0.sca \
	results/pulp-nc-rate-evts$(EQ)50,ltype$(EQ)average-\#0.sca
	bash -c "./scripts/network_evol pulp-orig-rate evts ltype "algo,param,atomic,bytes,latency_distri,gensize" > data/fig5_rate.csv"
	bash -c "./scripts/network_evol pulp-nc-rate evts ltype >> data/fig5_rate.csv"

data/fig5_latency.csv: \
	results/pulp-orig-latency-ltype$(EQ)average,lmul$(EQ)0.1-\#0.sca \
	results/pulp-nc-latency-ltype$(EQ)average,lmul$(EQ)0.1-\#0.sca
	bash -c "./scripts/network_evol pulp-orig-latency ltype lmul "algo,param,atomic,bytes,latency_distri,gensize" > data/fig5_latency.csv"
	bash -c "./scripts/network_evol pulp-nc-latency ltype lmul >> data/fig5_latency.csv"

data/fig5_size.csv: \
	results/pulp-orig-size-nodes$(EQ)100,ltype$(EQ)average-\#0.sca \
	results/pulp-nc-size-nodes$(EQ)100,ltype$(EQ)average-\#0.sca
	bash -c "./scripts/network_evol pulp-orig-size nodes ltype "algo,param,atomic,bytes,latency_distri,gensize" > data/fig5_size.csv"
	bash -c "./scripts/network_evol pulp-nc-size nodes ltype >> data/fig5_size.csv"

## EXTRACT DATA FOR FIGURE 6

data/fig6_slow_%.csv: results/pulp-%-churn-cm$(EQ)0.0005-\#0.vec
	./scripts/extract_delay.py 1000 < $< > $@

data/fig6_mid_%.csv: results/pulp-%-churn-cm$(EQ)0.001-\#0.vec
	./scripts/extract_delay.py 1000 < $< > $@

data/fig6_fast_%.csv: results/pulp-%-churn-cm$(EQ)0.002-\#0.vec
	./scripts/extract_delay.py 1000 < $< > $@

## BUILD FIGURE 2 GRAPH

# @TODO

## BUILD FIGURE 3 GRAPH

tex/figure3-%.tex: r/figure3-%.R data/fig3_%_msgcount.csv data/fig3_%_msgdata.csv data/fig3_%_latency.csv
	mkdir -p ./tex
	cd r/ && Rscript $(notdir $<)

## BUILD FIGURE 4 GRAPH

tex/fig4_ghost_%.txt: \
	data/fig4_orig_%.csv \
	data/fig4_nc_%.csv \
	data/fig4_variant1_%.csv \
	data/fig4_variant2_%.csv
	touch $@

tex/figure4.tex: \
	tex/fig4_ghost_useful.txt \
	tex/fig4_ghost_curfreq.txt \
	tex/fig4_ghost_missinglog.txt
	mkdir -p ./tex
	cd r/ && Rscript figure4.R

## BUILD FIGURE 5 GRAPH

tex/figure5.tex: \
	data/fig5_rate.csv \
	data/fig5_latency.csv \
	data/fig5_size.csv
	mkdir -p ./tex
	cd r/ && Rscript figure5.R

## BUILD FIGURE 6 GRAPH

tex/figure6.tex: \
	data/fig6_slow_orig.csv \
	data/fig6_slow_nc.csv \
	data/fig6_mid_orig.csv \
	data/fig6_mid_nc.csv \
	data/fig6_fast_orig.csv \
	data/fig6_fast_nc.csv


## GENERATE A PDF

pdf/figures.pdf: \
	tex/wrapper.tex \
	tex/figure3-orig.tex \
	tex/figure3-nc.tex \
	tex/figure4.tex \
	tex/figure5.tex \
	tex/figure6.tex
	mkdir -p ./pdf
	cd tex/ && latexmk -pdf wrapper
	cp tex/wrapper.pdf pdf/figures.pdf

all: pdf/figures.pdf
