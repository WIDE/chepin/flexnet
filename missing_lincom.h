#pragma once
#include "build_config.h"
#include <map>
#include <algorithm>
#include <vector>
#include <set>

#include <flexcode/cpp/pool.hpp>
extern "C" {
#include "flexcode/c/pool.h"
}

class PushPullTradingGen;
class MissingLinCom {
  protected:
    PushPullTradingGen* dm;
    std::vector<uint32_t> missing_gen;
    std::map<uint32_t, uint16_t> report;
  public:
    std::set<uint16_t> seen;
    MissingLinCom(PushPullTradingGen* dm) : dm(dm) {}
    void inform(uint32_t gid);
    void add_packet(pool_packet_t& pp);
    void add_msg(uint32_t gid, uint16_t mid);
    void add(std::map<uint32_t, std::vector<uint16_t>> trading);
    uint64_t size();
    void populate(std::vector<uint32_t>& a);
};
