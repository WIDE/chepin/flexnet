#include "push-pull.h"

// The module class needs to be registered with OMNeT++
Define_Module(PushPull);

PushPull::PushPull()
{
  miss = new MissingSimple(this);
}

PushPull::~PushPull()
{
  delete miss;
}

void PushPull::initialize()
{
  DisseminationModule::initialize();

  prev_missing_size = 0;
  prev_useless = 0;
  prev_useful = 0;
  deltaPull = getParentModule()->par("rounds_delta").doubleValue();
  deltaFreq = -1;
  tradingLength = getParentModule()->par("trading_length").intValue();
  tradingMargin = getParentModule()->par("trading_margin").intValue();

  cMessage* clock = new cMessage("periodic-pull");
  scheduleAt(0, clock);

  if (getParentModule()->par("adaptive").intValue() != 0) {
    deltaFreq = getParentModule()->par("rounds_delta_update").doubleValue();
    cMessage* clock = new cMessage("adapt-freq");
    scheduleAt(uniform(0, deltaFreq), clock);
  }

  #if LOG_USEFUL_USELESS
  useful.setName("useful");
  useless.setName("useless");
  #endif

  #if LOG_FREQ
  reset_freq.setName("reset_freq");
  increase_freq.setName("increase_freq");
  decrease_freq.setName("decrease_freq");
  current_freq.setName("current_freq");
  #endif

  #if LOG_USELESSNESS
  useless_unknown.setName("useless_unknown");
  useless_redundant.setName("useless_redundant");
  #endif
  
  #if MISSING_LOG
  missing_log.setName("missing_log");
  #endif
}

void PushPull::periodicPull()
{
  PullPacket* p = new PullPacket("pull");
  p->setKind(1);
  miss->populate(p->getMissing());
  populateTradingWindow(p->getTradingWindow());

  auto target = rps();
  //double latency = std::max(0., normal(o->latency_avg, o->latency_stddev));
  double latency = o->get_latency(getIndex(), target->getIndex());
  
  sendDirect(p, latency, 0., target, "in");
  #if DEBUG_SENT_PULL
  std::cout << "Missing size: " << p->getMissing().size() << "(" << p->getMissing().size() * MSG_ID_SIZE << " bytes)" << std::endl;
  std::cout << "Trading size: " << p->getTradingWindow().size() << "(" << p->getTradingWindow().size() * MSG_ID_SIZE << " bytes)" << std::endl;
  #endif
  sent_packet_pull(p->getMissing().size() * MSG_ID_SIZE + p->getTradingWindow().size() * MSG_ID_SIZE);
}

void PushPull::adaptFrequency()
{
  if (miss->size() > prev_missing_size) {
    deltaPull = deltaFreq / (double)(miss->size() - prev_missing_size + prev_useful);

    #if DEBUG_FREQ
    if(getIndex() == 0) std::cout << "resetting deltaPull to " << deltaPull << std::endl;
    #endif
    #if LOG_FREQ
    reset_freq.record(1);
    #endif
  } else {
    if (miss->size() > 0 && prev_useless <= prev_useful) {
      deltaPull *= 0.9;

      #if DEBUG_FREQ
      if(getIndex() == 0) std::cout << "increasing deltaPull frequency to " << deltaPull << "sec" << std::endl;
      #endif
      #if LOG_FREQ
      increase_freq.record(1);
      #endif
    } else {
      deltaPull *= 1.1;

      #if DEBUG_FREQ
      if(getIndex() == 0) std::cout << "lowering deltaPull frequency to " << deltaPull << "sec" << std::endl;
      #endif
      #if LOG_FREQ
      decrease_freq.record(1);
      #endif
    }
  }

  deltaPull = std::min(std::max(deltaPull.dbl(), o->rounds_delta / 100), o->rounds_delta * 100);

  #if DEBUG_FREQ
  if (getIndex() == 0) {
    std::cout << "Missing -> current: " << miss->size() << ", prev: " << prev_missing_size << std::endl;
    std::cout << "Pull -> useless: " << prev_useless << ", useful: " << prev_useful << std::endl;
    std::cout << "Retained pull frequency -> " << deltaPull << std::endl << std::endl;
  }
  #endif
  
  prev_useless = 0;
  prev_useful = 0;
  prev_missing_size = miss->size();
  #if LOG_FREQ
  current_freq.record(deltaPull);
  #endif
}

void PushPull::set_received2(uint16_t id) 
{
  // Add packet to recent history
  recent_history.push_back(id);
  if (recent_history.size() > tradingLength + tradingMargin) {
    recent_history.pop_front();
  }

  // Remove it from missing list
  miss->received(id); 

  // Log it as received
  set_received(id);
}

void PushPull::inject(uint16_t id)
{
  PushPacket* msg = new PushPacket("push");
  msg->setPktId(id);
  msg->setTtl(o->ttl);
  msg->setKind(0);
  forward(msg);
}

void PushPull::forward(cMessage *msg) {
  if (is_down) {
    delete msg;
    return;
  }
  
  if (strcmp(msg->getName(), "push") == 0) {
    push((PushPacket*)msg);
  } else if (strcmp(msg->getName(), "pull") == 0) {
    pull((PullPacket*)msg);
  } else if (strcmp(msg->getName(), "reply-pull") == 0) {
    pullReply((PullReplyPacket*)msg);
  }
}

void PushPull::push(PushPacket* msg)
{
  uint16_t pkt_id = msg->getPktId();
  
  // Look at trading window and add new missing packets id
  miss->add(msg->getTradingWindow());
  #if MISSING_LOG
  missing_log.record(miss->size());
  #endif
  
  // Look if we have already received the packet
  if (is_received(pkt_id)) {
    delete msg;
    return;
  }

  // If not, add the packet to the received list and the recent history list
  set_received2(pkt_id);

  if (msg->getTtl() <= 0) {
    delete msg;
    return;
  }
  // Decrement the TTL
  msg->setTtl(msg->getTtl() - 1);

  // The TTL is not 0, packet will be forwarded
  // Update the trading window with a part of our recent_history
  msg->getTradingWindow().clear();
  populateTradingWindow(msg->getTradingWindow());

  for (int i = 0; i < o->fanout; i++) {
    auto target = rps();
    //double latency = std::max(0., normal(o->latency_avg, o->latency_stddev));
    double latency = o->get_latency(getIndex(),target->getIndex());
    sendDirect(msg->dup(), latency, 0., target, "in");
    sent_packet_push(1000+ MSG_ID_SIZE +1+msg->getTradingWindow().size() * MSG_ID_SIZE);
  }
  
  delete msg; 
}

void PushPull::populateTradingWindow(std::vector<uint16_t>& trading) {
  for (auto it = recent_history.begin();
       it != recent_history.end() - std::min(tradingMargin, recent_history.size());
       ++it) {
    trading.push_back(*it);
  }
}

void PushPull::pull(PullPacket* msg)
{
  miss->add(msg->getTradingWindow());
  #if MISSING_LOG
  missing_log.record(miss->size());
  #endif

  // Find first missing packet
  uint16_t selected_id = UINT16_MAX;
  for (auto const& ent : msg->getMissing()) {
    if (is_received(ent)) {
      selected_id = ent;
      break;
    }
  }
  
  PullReplyPacket* pr = new PullReplyPacket("reply-pull");
  pr->setMissingPktId(selected_id);
  pr->setKind(2);
  populateTradingWindow(pr->getTradingWindow());
 
  auto target = msg->getSenderModule();
  //double latency = std::max(0., normal(o->latency_avg, o->latency_stddev));
  double latency = o->get_latency(getIndex(), target->getIndex());
  sendDirect(pr, latency, 0., target, "in");
  if (selected_id == UINT16_MAX) {
    sent_packet_pull_reply(MSG_ID_SIZE + pr->getTradingWindow().size() * MSG_ID_SIZE);
  } else {
    sent_packet_pull_reply(MSG_ID_SIZE + 1000 + pr->getTradingWindow().size() * MSG_ID_SIZE);
  }
  delete msg;
}

void PushPull::pullReply(PullReplyPacket* msg)
{
  uint16_t rcv_id = msg->getMissingPktId();
  miss->add(msg->getTradingWindow());
  #if MISSING_LOG
  missing_log.record(miss->size());
  #endif
  delete msg;

  if (rcv_id == UINT16_MAX || is_received(rcv_id)) { 
    prev_useless++;
    #if LOG_USEFUL_USELESS
    useless.record(1);
    #endif

    #if LOG_USELESSNESS
    if (rcv_id == UINT16_MAX) useless_unknown.record(1);
    else useless_redundant.record(1);
    #endif
  } else {
    prev_useful++;
    #if LOG_USEFUL_USELESS 
    useful.record(1);
    #endif

    set_received2(rcv_id);
  }
}

void PushPull::background(cMessage* msg)
{
  if (strcmp(msg->getName(), "periodic-pull") == 0) {
    if (!is_down) periodicPull();
    scheduleAt(simTime()+deltaPull, msg);
  } else if (strcmp(msg->getName(), "adapt-freq") == 0) {
    if (!is_down) adaptFrequency();
    scheduleAt(simTime()+deltaFreq, msg);
  } 
}
