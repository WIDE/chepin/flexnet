#include <string.h>
#include <algorithm>
#include <omnetpp.h>
#include <stdint.h>
#include <map>
#include "simple_packet_m.h"
#include "base.h"

using namespace omnetpp;

class BallsAndBins : public DisseminationModule
{
  protected:
    std::map<uint16_t, TTLPacket*> buffer;
    cMessage* clock;
    ~BallsAndBins();
    virtual void initialize() override;
    virtual void inject(uint16_t id) override;
    virtual void forward(cMessage *msg) override;
    virtual void background(cMessage *msg) override;
};

// The module class needs to be registered with OMNeT++
Define_Module(BallsAndBins);

BallsAndBins::~BallsAndBins()
{
  for (auto const& ent : buffer) {
    delete ent.second;
  }
}

void BallsAndBins::initialize()
{
  DisseminationModule::initialize();
  clock = new cMessage("tick");
  scheduleAt(0., clock);
}

void BallsAndBins::inject(uint16_t id)
{
  TTLPacket* p = new TTLPacket("s");
  p->setPktId(id);
  p->setTtl(o->ttl);
  p->setKind(id % 8);
  forward(p);
}

void BallsAndBins::forward(cMessage *msg)
{
  TTLPacket* p = (TTLPacket*)msg;
  uint16_t pkt_id = p->getPktId();
  if (!is_received(pkt_id)) {
    set_received(pkt_id);
  }
 
  if (buffer.find(pkt_id) != buffer.end()) {
    int min_ttl = std::min(p->getTtl(), buffer[pkt_id]->getTtl());
    buffer[pkt_id]->setTtl(min_ttl); 
    delete p;
    return;
  }

  buffer[pkt_id] = p;
}

void BallsAndBins::background(cMessage *msg)
{
  scheduleAt(simTime()+o->rounds_delta, clock);
  auto iter = buffer.begin();
  while (iter != buffer.end()) {
    int new_ttl = iter->second->getTtl() - 1;
    if (new_ttl == 0) {
      delete iter->second;
      iter = buffer.erase(iter);
      continue;
    }
    iter->second->setTtl(new_ttl);
    iter++;
  }

  if (buffer.size() <= 0) return;

  for (int i = 0; i < o->fanout; i++) {
    cModule *targetModule = rps();
    double latency = std::max(0., normal(o->latency_avg, o->latency_stddev));
    for (auto const& ent : buffer) {
      sendDirect(ent.second->dup(), latency, 0., targetModule, "in");
      sent_packet(1000+4+1);
    }
  }

  for (auto const& ent : buffer) {
    delete ent.second;
  }
  buffer.clear();
}
