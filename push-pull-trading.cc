#include <flexcode/cpp/pool.hpp>
#include <omnetpp.h>
#include <string.h>
#include <stdint.h>
#include <iterator>
#include <deque>
#include "base.h"
#include "missing_simple.h"
#include "push_pull_m.h"
#include "nc_packet_m.h"
#define MSG_ID_SIZE 8

using namespace omnetpp;

class PushPullTrading : public DisseminationModule
{
  public:
    PushPullTrading() { miss = new MissingSimple(this); }
    ~PushPullTrading() { delete miss; }

    virtual void deliver(unsigned char* msg, uint16_t len);
    virtual void send(pool_packet_t* pkts, uint8_t len);
  protected:
    std::deque<uint16_t> recent_history;
    Missing* miss;
    Pool localPool;
    int union_coef;
    int shuffle;
    
    simtime_t deltaPull; //deltaPull
    simtime_t deltaFreq; //deltaAdjust
    
    uint32_t prev_missing_size;
    uint32_t prev_useful;
    uint32_t prev_useless;

    uint64_t tradingLength;
    uint64_t tradingMargin;

    virtual void initialize() override;
    virtual void inject(uint16_t id) override;
    virtual void forward(cMessage *msg) override;
    virtual void background(cMessage *msg) override;
    virtual void finish() override;

    void adaptFrequency();
    void periodicPull();
    
    void pull(PullPacket* msg);
    void pullReply(NCPacket2* msg);
    void push(NCPacket2* msg);

    void set_received2(uint16_t id);
    void populateTradingWindow(std::vector<uint16_t>& trading);
};

Define_Module(PushPullTrading);

/* C CALLBACK ***********************************/

void cdeliver3(void* handle, unsigned char* msg, uint16_t len) {
  ((PushPullTrading*)handle)->deliver(msg, len);
}

void csend3(void* handle, pool_packet_t* pkts, uint8_t len) {
  ((PushPullTrading*)handle)->send(pkts, len);
}

/* NETWORK CODING CALLBACKS *********************/

void PushPullTrading::deliver(unsigned char* msg, uint16_t len)
{
  uint16_t num = *((uint16_t*)msg);
  set_received2(num);
}

void PushPullTrading::send(pool_packet_t* pkts, uint8_t len)
{
  for (int i = 0; i < len; i++) {
    cModule *target = rps();

    pool_packet_t pp;
    pool_packet_deep_cpy(&pp, &(pkts[i]));
    
    NCPacket2* np = new NCPacket2("push");
    np->setNcPkt(pp);
    populateTradingWindow(np->getTradingWindow());

    sent_packet_push(1000+1+5*pp.coefs_count+4+np->getTradingWindow().size() * MSG_ID_SIZE);
    sendDirect(np, std::max(0., normal(o->latency_avg, o->latency_stddev)), 0., target, "in");
  }
}

/* GENERAL FX ***********************************/

void PushPullTrading::initialize()
{
  DisseminationModule::initialize();
  
  int fanout = getParentModule()->par("fanout").intValue();
  int ttl = getParentModule()->par("ttl").intValue();
  localPool.init((void*)this, cdeliver3, csend3, fanout, 1, 1, 0, POL_OCTAVE, (uint8_t)ttl);

  int rotation = getParentModule()->par("missing_rotation").intValue();
  MissingSimple* ms = static_cast<MissingSimple*>(miss);
  ms->set_rotation(rotation);
  ms->set_shuffle(shuffle == 1);

  union_coef = getParentModule()->par("union_coef").intValue();
  shuffle = getParentModule()->par("shuffle").intValue();
  tradingLength = getParentModule()->par("trading_length").intValue();
  tradingMargin = getParentModule()->par("trading_margin").intValue();

  prev_missing_size = 0;
  prev_useless = 0;
  prev_useful = 0;
  deltaPull = getParentModule()->par("rounds_delta").doubleValue();
  deltaFreq = -1;

  cMessage* clock = new cMessage("periodic-pull");
  scheduleAt(0, clock);

  if (getParentModule()->par("adaptive").intValue() != 0) {
    deltaFreq = getParentModule()->par("rounds_delta").doubleValue();
    cMessage* clock = new cMessage("adapt-freq");
    scheduleAt(0, clock);
  }
}

void PushPullTrading::inject(uint16_t idx)
{
  set_received2(idx);
  // The content of the message is also the message ID, but we suppose a 1KB message content
  o->msgid_to_generation[idx] = localPool.current_generation();
  localPool.broadcast(idx, (unsigned char*)&idx, (uint16_t)sizeof(uint16_t));
}

void PushPullTrading::forward(cMessage *msg) {
  if (strcmp(msg->getName(), "push") == 0) {
    push((NCPacket2*)msg);
  } else if (strcmp(msg->getName(), "pull") == 0) {
    pull((PullPacket*)msg);
  } else if (strcmp(msg->getName(), "reply-pull") == 0) {
    pullReply((NCPacket2*)msg);
  }
}

void PushPullTrading::background(cMessage* msg)
{
  if (strcmp(msg->getName(), "periodic-pull") == 0) {
    periodicPull();
    scheduleAt(simTime()+deltaPull, msg);
  } else if (strcmp(msg->getName(), "adapt-freq") == 0) {
    adaptFrequency();
    scheduleAt(simTime()+deltaFreq, msg);
  } 
}

void PushPullTrading::finish()
{
  if (getIndex() != 0) return;

  cOutVector genv("Generation Sizes Vector");
  cStdDev gen("Generation Sizes");
  for (auto const &size : localPool.stat_generations_size()) {
    if (size == 0) continue;
    genv.record(size);
    gen.collect(size);
  }
  recordScalar("Number of generations", gen.getCount());
  recordScalar("Mean generation sizes", gen.getMean());
  recordScalar("Standard deviation of generation sizes", gen.getStddev());
  recordScalar("Minimum generation size", gen.getMin());
  recordScalar("Maximum generation size", gen.getMax());
}

/* BACKGROUND THREADS ***************************/
void PushPullTrading::periodicPull()
{
  PullPacket* p = new PullPacket("pull");
  p->setKind(1);
  miss->populate(p->getMissing());
  populateTradingWindow(p->getTradingWindow());

  double latency = std::max(0., normal(o->latency_avg, o->latency_stddev));
  #if DEBUG_SENT_PULL
  std::cout << "Missing size: " << p->getMissing().size() << "(" << p->getMissing().size() * MSG_ID_SIZE << " bytes)" << std::endl;
  std::cout << "Trading size: " << p->getTradingWindow().size() << "(" << p->getTradingWindow().size() * MSG_ID_SIZE << " bytes)" << std::endl;
  #endif
  sent_packet_pull(p->getMissing().size() * MSG_ID_SIZE + p->getTradingWindow().size() * MSG_ID_SIZE);
  sendDirect(p, latency, 0., rps(), "in");
}

void PushPullTrading::adaptFrequency()
{
  if (miss->size() > prev_missing_size) {
    deltaPull = deltaFreq / (double)(miss->size() - prev_missing_size + prev_useful);
    
    #if DEBUG_FREQ
    if(getIndex() == 0) std::cout << "resetting deltaPull to " << deltaPull << std::endl;
    #endif
  } else {
    if (miss->size() > 0 && prev_useless <= prev_useful) {
      deltaPull *= 0.9;
      
      #if DEBUG_FREQ
      if(getIndex() == 0) std::cout << "increasing deltaPull frequency to " << deltaPull << "sec" << std::endl;
      #endif
    } else {
      deltaPull *= 1.1;
      
      #if DEBUG_FREQ
      if(getIndex() == 0) std::cout << "lowering deltaPull frequency to " << deltaPull << "sec" << std::endl;
      #endif
    }
  }

  deltaPull = std::min(std::max(deltaPull.dbl(), o->rounds_delta / 100), o->rounds_delta * 100);
  #if DEBUG_FREQ
  if (getIndex() == 0) {
    std::cout << "Missing -> current: " << miss->size() << ", prev: " << prev_missing_size << std::endl;
    std::cout << "Pull -> useless: " << prev_useless << ", useful: " << prev_useful << std::endl;
    std::cout << "Retained pull frequency -> " << deltaPull << std::endl << std::endl;
  }
  #endif
  prev_useless = 0;
  prev_useful = 0;
  prev_missing_size = miss->size();
}

/* MESSAGE EXCHANGE *****************************/
void PushPullTrading::pull(PullPacket* msg)
{
  miss->add(msg->getTradingWindow());

  // Find first missing packet
  uint32_t selected_generation = UINT32_MAX;
  for (auto const& ent : msg->getMissing()) {
    selected_generation = localPool.get_message_generation(ent);
    if (selected_generation != UINT32_MAX) break;
  }
 
  NCPacket2* pr = new NCPacket2("reply-pull");
  pr->setKind(2);
  populateTradingWindow(pr->getTradingWindow());
  
  double latency = std::max(0., normal(o->latency_avg, o->latency_stddev));
  if (selected_generation != UINT32_MAX) {
    pool_packet_t pp2 = {0};
    localPool.recode(&pp2, selected_generation, 0);
    pr->setNcPkt(pp2);
    sent_packet_pull_reply(1 + 4 + pp2.coefs_count * sizeof(uint16_t) + 1000 + pr->getTradingWindow().size() * MSG_ID_SIZE);
  } else {
    pool_packet_t pp;
    pp.coefs_count = 0;
    pr->setNcPkt(pp);
    sent_packet_pull_reply(MSG_ID_SIZE + pr->getTradingWindow().size() * MSG_ID_SIZE);
  }

  sendDirect(pr, latency, 0., msg->getSenderModule(), "in");
  delete msg;
}

void PushPullTrading::pullReply(NCPacket2* msg)
{
  pool_packet_t p = msg->getNcPkt();

  miss->add(msg->getTradingWindow());
  if (union_coef) {
    std::vector<uint16_t> remoteWindow;
    for (int i = 0; i < p.coefs_count ; i++) {
      remoteWindow.push_back(p.coefs[i].msg_id);
    }
    miss->add(remoteWindow);
  }

  if(p.coefs_count > 0 && localPool.receive(&p)) prev_useful++;
  else prev_useless++;

  if (p.coefs_count > 0)
    pool_packet_free(&p);
  delete msg;
}

void PushPullTrading::push(NCPacket2 *m)
{
  pool_packet_t p = m->getNcPkt();
  
  miss->add(m->getTradingWindow());
  if (union_coef) {
    std::vector<uint16_t> remoteWindow;
    for (int i = 0; i < p.coefs_count ; i++) {
      remoteWindow.push_back(p.coefs[i].msg_id);
    }
    miss->add(remoteWindow);
  }

  localPool.receive(&p);
  pool_packet_free(&p);
  delete m;
}

/* UTILS ****************************************/
void PushPullTrading::set_received2(uint16_t id) 
{
  // Add packet to recent history
  recent_history.push_back(id);
  if (recent_history.size() > tradingMargin + tradingLength) {
    recent_history.pop_front();
  }

  // Remove it from missing list
  miss->received(id); 

  // Log it as received
  set_received(id);
}

void PushPullTrading::populateTradingWindow(std::vector<uint16_t>& trading)
{
  for (auto it = recent_history.begin();
       it != recent_history.end() - std::min(tradingMargin, recent_history.size());
       ++it) {
    trading.push_back(*it);
  }
}
