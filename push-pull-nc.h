#pragma once
#include <string.h>
#include <algorithm>
#include <omnetpp.h>
#include <stdint.h>
#include <deque>
#include <set>
#include <map>
#include <iterator>
#include <flexcode/cpp/pool.hpp>
#include "simple_packet_m.h"
#include "nc_packet_m.h"
#include "push-pull.h"

using namespace omnetpp;

class PushPullNC : public PushPull
{
  protected:
    std::map<uint32_t, uint32_t> known_missing;

    virtual void initialize() override;
    virtual void inject(uint16_t idx) override;
    virtual void forward(cMessage *msg) override;
    virtual void finish() override;
    
    void push(NCPacket* msg);
    virtual void pullReply(PullReplyPacket* msg) override;

  public:
    Pool localPool;
    virtual void deliver(unsigned char* msg, uint16_t len);
    virtual void send(pool_packet_t* pkts, uint8_t len);
};

