#include <string.h>
#include <algorithm>
#include <omnetpp.h>
#include <stdint.h>
#include <deque>
#include <set>
#include <iterator>
#include <numeric>
#include "simple_packet_m.h"
#include "push_pull_m.h"
#include "base.h"
#include "missing_simple.h"
#include "build_config.h"

using namespace omnetpp;

class PushPullControl : public DisseminationModule
{
  public:
    PushPullControl();
    ~PushPullControl();
  protected:
    std::deque<uint16_t> recent_history; //Hp
    Missing* miss;
    
    /* ADAPTATION */
    simtime_t deltaPull; //deltaPull
    simtime_t lastAdaptation;
    uint8_t rcv_samples;
    uint8_t samples;
    uint8_t cpid;
    uint16_t useful_count;
    uint16_t total;
    double errSum;
    int64_t last_missing;
    double last_error;
    cOutVector rate;

    #if LOG_USEFUL_USELESS
    cOutVector useful;
    cOutVector useless;
    #endif
    
    #if LOG_FREQ
    cOutVector reset_freq;
    cOutVector increase_freq;
    cOutVector decrease_freq;
    cOutVector current_freq;
    #endif

    #if LOG_USELESSNESS
    cOutVector useless_unknown;
    cOutVector useless_redundant;
    #endif
  
    #if MISSING_LOG
    cOutVector missing_log;
    #endif
    
    uint32_t prev_missing_size;
    uint32_t prev_useful;
    uint32_t prev_useless;

    uint64_t tradingLength;
    uint64_t tradingMargin;

    virtual void initialize() override;
    virtual void inject(uint16_t id) override;
    virtual void forward(cMessage *msg) override;
    virtual void background(cMessage *msg) override;

    virtual void push(PushPacket* msg);
    virtual void pull(PullPacket* msg);
    virtual void pullReply(PullReplyPacket* msg);

    // @FIXME should be refactored to override set_received
    void set_received2(uint16_t id);

    virtual void periodicPull();
    void populateTradingWindow(std::vector<uint16_t>& trading);
};
