#include "push-pull-trading-gen.h"

Define_Module(PushPullTradingGen);

/* C CALLBACK ***********************************/

void cdeliver4(void* handle, unsigned char* msg, uint16_t len) {
  ((PushPullTradingGen*)handle)->deliver(msg, len);
}

void csend4(void* handle, pool_packet_t* pkts, uint8_t len) {
  ((PushPullTradingGen*)handle)->send(pkts, len);
}

/* NETWORK CODING CALLBACKS *********************/

void PushPullTradingGen::deliver(unsigned char* msg, uint16_t len)
{
  uint16_t num = *((uint16_t*)msg);
  set_received2(num);
}

void PushPullTradingGen::send(pool_packet_t* pkts, uint8_t len)
{
  for (int i = 0; i < len; i++) {
    cModule *target = rps();

    pool_packet_t pp;
    pool_packet_deep_cpy(&pp, &(pkts[i]));
    
    NCPacket3* np = new NCPacket3("push");
    np->setNcPkt(pp);
    uint64_t tradingSize = populateTradingWindow(np->getTradingWindow());

    sent_packet_push(1000 + 1 + 5*pp.coefs_count + 4 + tradingSize);
    //double latency = std::max(0., normal(o->latency_avg, o->latency_stddev));
    double latency = o->get_latency(getIndex(), target->getIndex());
    sendDirect(np, latency, 0., target, "in");
  }
}

/* GENERAL FX ***********************************/

void PushPullTradingGen::initialize()
{
  DisseminationModule::initialize();
  
  int fanout = getParentModule()->par("fanout").intValue();
  int ttl = getParentModule()->par("ttl").intValue();
  localPool.init((void*)this, cdeliver4, csend4, fanout, 1, 1, 0, POL_OCTAVE, (uint8_t)ttl);

  union_coef = getParentModule()->par("union_coef").intValue();
  tradingLength = getParentModule()->par("trading_length").intValue();
  tradingMargin = getParentModule()->par("trading_margin").intValue();

  prev_missing_size = 0;
  prev_useless = 0;
  prev_useful = 0;
  deltaPull = getParentModule()->par("rounds_delta").doubleValue();
  deltaFreq = -1;

  cMessage* clock = new cMessage("periodic-pull");
  scheduleAt(0, clock);

  if (getParentModule()->par("adaptive").intValue() != 0) {
    deltaFreq = getParentModule()->par("rounds_delta").doubleValue();
    cMessage* clock = new cMessage("adapt-freq");
    scheduleAt(uniform(0, deltaFreq), clock);
  }

  #if LOG_USEFUL_USELESS
  useful.setName("useful");
  useless.setName("useless");
  #endif 

  #if LOG_FREQ
  reset_freq.setName("reset_freq");
  increase_freq.setName("increase_freq");
  decrease_freq.setName("decrease_freq");
  current_freq.setName("current_freq");
  #endif

  #if LOG_USELESSNESS
  useless_unknown.setName("useless_unknown");
  useless_redundant.setName("useless_redundant");
  #endif
  
  #if MISSING_LOG
  missing_log.setName("missing_log");
  #endif
}

void PushPullTradingGen::inject(uint16_t idx)
{
  set_received2(idx);
  // The content of the message is also the message ID, but we suppose a 1KB message content
  auto gid = localPool.current_generation();
  o->msgid_to_generation[idx] = gid;
  localPool.broadcast(idx, (unsigned char*)&idx, (uint16_t)sizeof(uint16_t));
  miss->add_msg(gid, idx);
}

void PushPullTradingGen::forward(cMessage *msg) {
  if (is_down) {
    delete msg;
    return;
  }

  if (strcmp(msg->getName(), "push") == 0) {
    push((NCPacket3*)msg);
  } else if (strcmp(msg->getName(), "pull") == 0) {
    pull((GenPullPacket*)msg);
  } else if (strcmp(msg->getName(), "reply-pull") == 0) {
    pullReply((NCPacket3*)msg);
  }
}

void PushPullTradingGen::background(cMessage* msg)
{
  if (strcmp(msg->getName(), "periodic-pull") == 0) {
    if (!is_down) periodicPull();
    scheduleAt(simTime()+deltaPull, msg);
  } else if (strcmp(msg->getName(), "adapt-freq") == 0) {
    if (!is_down) adaptFrequency();
    scheduleAt(simTime()+deltaFreq, msg);
  } 
}

void PushPullTradingGen::finish()
{  
  #if DEBUG_MISSED_MESSAGES

  for (int i = 0; i < 1000; i++) {
    if(!is_received(i)) {
      auto gen = o->msgid_to_generation[i];
      std::cout << getIndex() << " missed " << i << " from generation " << gen << " emited at " << o->emit_time[i] << std::endl;
      if (o->rcv_instead_dlv) {
        if (miss->seen.find(i) != miss->seen.end()) std::cout << "existence of " << i << " is known by our node" <<std::endl;
        else std::cout << "existence of " << i << " is **NOT** known by our node" <<std::endl;   
      }
    }
  }
  #endif

  if (getIndex() != 0) return;
  //genkv_print(localPool.get_gen(0));

  cOutVector genv("Generation Sizes Vector");
  cStdDev gen("Generation Sizes");
  for (auto const &size : localPool.stat_generations_size()) {
    if (size == 0) continue;
    genv.record(size);
    gen.collect(size);
  }

  recordScalar("Number of generations", gen.getCount());
  recordScalar("Mean generation sizes", gen.getMean());
  recordScalar("Standard deviation of generation sizes", gen.getStddev());
  recordScalar("Minimum generation size", gen.getMin());
  recordScalar("Maximum generation size", gen.getMax());
}

/* BACKGROUND THREADS ***************************/
void PushPullTradingGen::periodicPull()
{
  GenPullPacket* p = new GenPullPacket("pull");
  p->setKind(1);
  miss->populate(p->getMissing());
  uint64_t tradingSize = populateTradingWindow(p->getTradingWindow());

  //double latency = std::max(0., normal(o->latency_avg, o->latency_stddev));
  auto target = rps();
  double latency = o->get_latency(getIndex(), target->getIndex());

  #if DEBUG_SENT_PULL
  std::cout << "Missing size: " << p->getMissing().size() << "(" << p->getMissing().size() * sizeof(uint32_t) << " bytes)" << std::endl;
  std::cout << "Trading size: " << p->getTradingWindow().size() << "(" << tradingSize << " bytes)" << std::endl;
  #endif

  sent_packet_pull(p->getMissing().size() * sizeof(uint32_t) + tradingSize);
  sendDirect(p, latency, 0., target, "in");
}

void PushPullTradingGen::adaptFrequency()
{
  if (miss->size() > prev_missing_size) {
    deltaPull = deltaFreq / (double)(miss->size() - prev_missing_size + prev_useful);
    
    #if DEBUG_FREQ
    if(getIndex() == 0) std::cout << "resetting deltaPull to " << deltaPull << std::endl;
    #endif
    #if LOG_FREQ
    reset_freq.record(1);
    #endif
  } else {
    if (miss->size() > 0 && prev_useless <= prev_useful) {
      deltaPull *= 0.9;
      
      #if DEBUG_FREQ
      if(getIndex() == 0) std::cout << "increasing deltaPull frequency to " << deltaPull << "sec" << std::endl;
      #endif
      #if LOG_FREQ
      increase_freq.record(1);
      #endif
    } else {
      deltaPull *= 1.1;
      
      #if DEBUG_FREQ
      if(getIndex() == 0) std::cout << "lowering deltaPull frequency to " << deltaPull << "sec" << std::endl;
      #endif
      #if LOG_FREQ
      decrease_freq.record(1);
      #endif
    }
  }

  deltaPull = std::min(std::max(deltaPull.dbl(), o->rounds_delta / 100), o->rounds_delta * 100);
  #if DEBUG_FREQ
  if (getIndex() == 0) {
    std::cout << "Missing -> current: " << miss->size() << ", prev: " << prev_missing_size << std::endl;
    std::cout << "Pull -> useless: " << prev_useless << ", useful: " << prev_useful << std::endl;
    std::cout << "Retained pull frequency -> " << deltaPull << std::endl << std::endl;
  }
  #endif
  prev_useless = 0;
  prev_useful = 0;
  prev_missing_size = miss->size();
  #if LOG_FREQ
  current_freq.record(deltaPull);
  #endif
}

/* MESSAGE EXCHANGE *****************************/
void PushPullTradingGen::pull(GenPullPacket* msg)
{
  miss->add(msg->getTradingWindow());
  #if MISSING_LOG
  missing_log.record(miss->size());
  #endif

  // Find first missing packet
  uint32_t selected_generation = UINT32_MAX;
  for (auto const& gen : msg->getMissing()) {
    genkv_t* gen_ptr = localPool.get_gen(gen);
    if (gen_ptr != NULL && gen_ptr->known_ids_count > 0) {
      selected_generation = gen;
    }
  }
 
  NCPacket3* pr = new NCPacket3("reply-pull");
  pr->setKind(2);
  uint64_t tradingSize = populateTradingWindow(pr->getTradingWindow());
  
  //double latency = std::max(0., normal(o->latency_avg, o->latency_stddev));
  double latency = o->get_latency(getIndex(), msg->getSenderModule()->getIndex());

  if (selected_generation != UINT32_MAX) {
    pool_packet_t pp2 = {0};
    localPool.recode(&pp2, selected_generation, 0);
    pr->setNcPkt(pp2);
    sent_packet_pull_reply(1 + 4 + pp2.coefs_count * 5 + 1000 + tradingSize);
  } else {
    pool_packet_t pp;
    pp.coefs_count = 0;
    pr->setNcPkt(pp);
    sent_packet_pull_reply(MSG_ID_SIZE + tradingSize);
  }

  sendDirect(pr, latency, 0., msg->getSenderModule(), "in");
  delete msg;
}

void PushPullTradingGen::pullReply(NCPacket3* msg)
{
  pool_packet_t p = msg->getNcPkt();

  miss->add(msg->getTradingWindow());
  #if MISSING_LOG
  missing_log.record(miss->size());
  #endif
  if (union_coef) {
    std::map<uint32_t, std::vector<uint16_t>> remoteWindow;
    for (int i = 0; i < p.coefs_count ; i++) {
      remoteWindow[p.gid].push_back(p.coefs[i].msg_id);
    }
    miss->add(remoteWindow);
    #if MISSING_LOG
    missing_log.record(miss->size());
    #endif
  }

  if(p.coefs_count > 0 && localPool.receive(&p)) {
    #if LOG_USEFUL_USELESS 
    useful.record(1);
    #endif
    prev_useful++;
    miss->add_packet(p);
  } else {
    #if LOG_USEFUL_USELESS
    useless.record(1);
    #endif

    #if LOG_USELESSNESS
    if (p.coefs_count == 0) {
      useless_unknown.record(1);
    } else {
      useless_redundant.record(1);
      /*
      if (getIndex() == 0) {
	std::cout << "<== debug ==>" << std::endl;
	std::cout << "gen: " << p.gid << std::endl;
        std::cout << "sent coefs count: " << p.coefs_count << std::endl;
	int c = 0;
	for (int i = 0; i < p.coefs_count ; i++) {
	  if (miss->is_in(p.coefs[i].msg_id)) c++;
	}
	std::cout << "number of missing id in the message: " << c << std::endl;

	int g = 0;
        for (auto const& ent : miss->data) {
	  if (ent.second == p.gid) {
	    g++;
	  }
	}
	std::cout << "missing messages in this generation: " << g << std::endl;
      }*/
    }
    #endif
    prev_useless++;
  }

  if (p.coefs_count > 0)
    pool_packet_free(&p);
  delete msg;
}

void PushPullTradingGen::push(NCPacket3 *m)
{
  pool_packet_t p = m->getNcPkt();
  
  miss->add(m->getTradingWindow());
  #if MISSING_LOG
  missing_log.record(miss->size());
  #endif
  if (union_coef) {
    std::map<uint32_t, std::vector<uint16_t>> remoteWindow;
    for (int i = 0; i < p.coefs_count ; i++) {
      remoteWindow[p.gid].push_back(p.coefs[i].msg_id);
    }
    miss->add(remoteWindow);
    #if MISSING_LOG
    missing_log.record(miss->size());
    #endif
  }

  localPool.receive(&p);
  miss->add_packet(p);
  pool_packet_free(&p);
  
  delete m;
}

/* UTILS ****************************************/

void PushPullTradingGen::update_trading(uint16_t id)
{
  // Add packet to recent history
  recent_history.push_back(id);
  if (recent_history.size() > tradingMargin + tradingLength) {
    recent_history.pop_front();
  }
}

void PushPullTradingGen::set_received2(uint16_t id) 
{
  if (!o->rcv_instead_dlv) {
    // Add packet to recent history
    recent_history.push_back(id);
    if (recent_history.size() > tradingMargin + tradingLength) {
      recent_history.pop_front();
    }
  }

  // Log it as received
  set_received(id);
}

uint64_t PushPullTradingGen::populateTradingWindow(std::map<uint32_t, std::vector<uint16_t>>& trading)
{
  uint64_t size = 0;
  for (auto it = recent_history.begin();
       it != recent_history.end() - std::min(tradingMargin, recent_history.size());
       ++it) {
    uint32_t gen = localPool.get_message_generation(*it);
    trading[gen].push_back(*it);
    size += sizeof(uint32_t); // 4 bytes messages id
  }
  size += sizeof(uint32_t) * trading.size(); // 4 bytes generation * number of generations
  return size;
}
