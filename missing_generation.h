#pragma once
#include <algorithm>
#include <map>
#include "missing.h"
#include "push-pull-nc.h"

class MissingGeneration : public Missing
{
  protected:
    PushPullNC* dm;
    uint16_t gen_shift;
    std::map<uint32_t, std::vector<uint16_t>> will_miss;
    std::vector<uint16_t> computed_missing;
    void check_generation();
  public:
    MissingGeneration(PushPullNC* dm, uint16_t gen_shift);
    virtual std::size_t size() override;
    virtual uint64_t add(std::vector<uint16_t>& a) override;
    virtual uint8_t received(uint16_t id) override;
    virtual bool is_in(uint16_t id) override;
    virtual void populate(std::vector<uint16_t>& a) override;
    virtual ~MissingGeneration() override;
};
