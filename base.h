#pragma once
#include <string.h>
#include <omnetpp.h>
#include <stdint.h>
#include <utility>
#include "build_config.h"
#include "oracle.h"
#include "simple_packet_m.h"

using namespace omnetpp;

class Oracle;

class DisseminationModule : public cSimpleModule
{
  protected:
    uint64_t* bm;

    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
    virtual void inject(uint16_t id) = 0;
    virtual void forward(cMessage *msg) = 0;
    virtual void background(cMessage *msg);
  public:
    Oracle* o;
    bool is_down;
    uint64_t sent_messages;
    uint64_t sent_data;
    uint64_t sent_push;
    uint64_t sent_push_data;
    uint64_t sent_pull;
    uint64_t sent_pull_data;
    uint64_t sent_pull_reply;
    uint64_t sent_pull_reply_data;
    #if LOG_EXCHANGE_OVER_TIME
    cOutVector v_sent_messages;
    cOutVector v_sent_data;
    cOutVector v_sent_push;
    cOutVector v_sent_push_data;
    cOutVector v_sent_pull;
    cOutVector v_sent_pull_data;
    cOutVector v_sent_pull_reply;
    cOutVector v_sent_pull_reply_data;
    #endif

    #if LOG_REDUNDANCY
    std::map<uint64_t, double> redundancy;
    #endif

    void sent_packet(uint32_t bytes);
    void sent_packet_push(uint32_t bytes);
    void sent_packet_pull(uint32_t bytes);
    void sent_packet_pull_reply(uint32_t bytes);

    void set_received(uint16_t pos);
    bool is_received(uint16_t pos);
    bool has_missed();
    int has_missed2();
    cModule* rps();

    ~DisseminationModule();
};
